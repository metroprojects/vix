#!/usr/bin/env python
# # -*- coding:iso-8859-1  -*-
#
# Vix : Visualization of XML
# ViX is Free and is provided by the Government of Canada
# Copyright (C) 2006 Environment Canada
#
#  Documentation: http://documentation.wikia.com/wiki/ViX
#
#
# Code contributed by:
#  Miguel Tremblay - Canadian meteorological center
#
#  $LastChangedDate$
#  $LastChangedRevision$
#
########################################################################
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
#

"""
Name:	       vix_png_match.py
Description: Generate the png graphics for the match file
 
Notes:

Author: Miguel Tremblay
Date: July 20th 2005
"""
import sys
sys.path.append('/users/dor/afsg/mit/svn/macadam/vix/src/ripp4x/')
import xgen_match_road_obs
from vix_ripp4x_match_road_obs import Vix_ripp4x_match_road_obs
from vix_match_road_obs import Vix_Match_Road_Obs

from vix_png import Vix_png

class Vix_png_match(Vix_png):

    sType = 'rover/'
    sSubType = 'match_road_obs/'
    sKeyword = 'rover'
    nStartDay = 27
    lElement = ['surface-temperature', 'road-condition',\
                'wind-speed',  'surface-accumulation']

    def __init__(self, lArgs):
        Vix_png.__init__(self, lArgs)

    def process_vix(self, sRoverPath, sPngFilename):
        vix_rov = Vix_Match_Road_Obs(sRoverPath)
        vix_rov.plot_all(sPngFilename)
        vix_rov.close()
        
    def process_ripp4x(self, sRoverPath, sRipp4xFilename, dPath):
        vix_ripp4x_match_road_obs = Vix_ripp4x_match_road_obs(sRipp4xFilename)
        vix_ripp4x_match_road_obs.process(sRoverPath)
        for sElement in list(self.dPath.keys()):
            vix_ripp4x_match_road_obs.set_path(sElement, \
                                               self.dPath[sElement])
        vix_ripp4x_match_road_obs.write_xml_file()

    def process_xgen(self, sRipp4xFilename, sSrcFilename):
        xgen = xgen_match_road_obs.Xgen_match_road_obs(sRipp4xFilename)
        xgen.process(sSrcFilename)
    

if __name__ == "__main__":
    import sys

    lArgs = ['']*4


    if sys.argv[1] == 'all':
        lStation = ['oav', 'ocy', 'oce', 'ocv', 'obw', 'oby']
    else:
        lStation = [sys.argv[1]]

    for sStation in lStation:
        for i in range(1, len(sys.argv)):
            lArgs[i-1] = sys.argv[i]
        lArgs[0] = sStation
        vix = Vix_png_match(lArgs)
        vix.process()



