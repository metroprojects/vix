#!/usr/bin/env python
# # -*- coding:iso-8859-1  -*-
#
# Vix : Visualization of XML
# ViX is Free and is provided by the Government of Canada
# Copyright (C) 2006 Environment Canada
#
#  Documentation: http://documentation.wikia.com/wiki/ViX
#
#
# Code contributed by:
#  Miguel Tremblay - Canadian meteorological center
#
#  $LastChangedDate$
#  $LastChangedRevision$
#
########################################################################
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
#

"""
 Name:	       rover_batch_road_obs.py
 Description: Generate the rover match files
 
 Notes: Ecrire un help digne de ce nom dans le main lorsque le fichier est
  appele sans argument.

 Author: Miguel Tremblay
 Date: September 23th 2005
"""

import os
import sys

from vix_png_rc import Vix_png_rc
from vix_png_match import Vix_png_match
from vix_png_score import Vix_png_score

sRoadcastDirectory = '/path/of/roadcast/xml/files'
sRoverMatch = '/path/of/roadcast_and_observation_merged/xml/files'


def main(lArgs):

    sType = lArgs[0]
    sStationArg = lArgs[1]
    sYearArg = lArgs[2]
    sMonthArg = lArgs[3]
    sDayArg = lArgs[4]

    if sType == 'roadcast':
        vix = Vix_png_rc(lArgs[1:])
        vix.process()
    elif sType == 'match':
        vix = Vix_png_match(lArgs[1:])
        vix.process()
    elif sType == 'score':
        vix = Vix_png_score(lArgs[1:])
        vix.process()
    else:
        sMessage = "Type '%s' is invalid. Possible choice are: 'roadcast' 'match' 'score'" % (sType)
        raise sMessage


#############################################################################
# Debug



if __name__ == "__main__":

    lArgs = ['']*6

    if len(sys.argv) < 3:
        print("Usage vix_batch.py [roadcast match score] station year month day")
        sys.exit(1)


    # There is one argument more when it is a score
    if sys.argv[1] == 'score':
        nStationIndice = 3
    else:
        nStationIndice = 2

    if sys.argv[nStationIndice] == 'all':
        lStation = os.listdir(sRoadcastDirectory)
        lStation.sort()
    else:
        lStation = [sys.argv[nStationIndice]]

    for sStation in lStation:
        for i in range(1, len(sys.argv)):
            lArgs[i-1] = sys.argv[i] 
        lArgs[nStationIndice-1] = sStation
        main(lArgs)



