#!/usr/bin/env python
# # -*- coding:iso-8859-1  -*-
#
# Vix : Visualization of XML
# ViX is Free and is provided by the Government of Canada
# Copyright (C) 2006 Environment Canada
#
#  Documentation: http://documentation.wikia.com/wiki/ViX
#
#
# Code contributed by:
#  Miguel Tremblay - Canadian meteorological center
#
#  $LastChangedDate$
#  $LastChangedRevision$
#
########################################################################
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
#

"""
Name:		vix_match
Description: Create the grapics for the match_road_obs files
 
Notes: Conversion pour GDD, 23 septembre 2005
Conversion pour Cairo, 5 ao�t 2014

Author: Miguel Tremblay
Date: May 3rd 2005
"""

import sys
import os
import time
import math
import rpy
from rpy import r
import numpy
import xml.utils.iso8601

import vix_util
from vix_graph import Vix_Graph

#_ = metro_util.init_translation('vix_roadcast_observation')
from gettext import gettext as _

class Vix_Match(Vix_Graph):

    # Attributs
    nYMinGraph = None
    nYMaxGraph = None
    nLineWidth = 2
    fPngWidthTemp = 640
    fPngHeightTemp = 500
    fPngWidthWS = 580
    fPngHeightWS = 380
    
    domMatch = None
    lX = None
    fLatitude = None
    fLongitude = None
    d_roadcast = {}
    d_color_roadcast = {}
    lColor = ['red', 'orange', 'cyan', 'green', 'violet', 'gray', 'olivedrab']
    dMainTitle = { 'at' : 'Air temperature' ,
                   'st' : 'Surface temperature',
                   'td' : 'Dew point'}


    Vix_MATCH_ERROR = ''
    
    def __init__(self, sFileMatch):
        self.d_color_roadcast = {}
        Vix_Graph.__init__(self)
        self.__load_dom(sFileMatch)
        self.__set_lat_long()

    def close(self):
        self.metro_xml_libxml2.free_dom(self.domMatch)
        
    def __load_dom(self, sPathMatch):
        """
        Create the DOM of the xml file.
        """

        if os.path.exists(sPathMatch):
            self.domMatch = self.metro_xml_libxml2.read_dom(sPathMatch)
        else:
            sNoFileError =  _("This file does not exist: %s") % (sPathMatch)
            raise sNoFileError

        self.d_roadcast = self.__get_roadcast(self.domMatch)

    def __get_roadcast(self, domMatch):
        """
        Find all the roadcast based on the first hour of forecast.
        Return a dictionnary with the date as the key and a concatenation
        of all the node with this date as the value.
        """
        
        d_roadcast = {}       # Get all nodes  'first-roadcast'
        lNode = self.metro_xml_libxml2.xpath(domMatch, \
                                             '//first-roadcast')

        for node in lNode:
            sDate = node.getContent()
            if sDate not in list(d_roadcast.keys()):
                node_roadcast_list = self.metro_xml_libxml2.create_node\
                                  (None, 'roadcast-list')
                self.metro_xml_libxml2.append_child\
                                     (node_roadcast_list,  \
                                      node.get_parent().copyNode(True))
                d_roadcast[sDate] = node_roadcast_list
            else:
                self.metro_xml_libxml2.append_child(d_roadcast[sDate], \
                                                    node.get_parent().copyNode(True))

        return d_roadcast


    def __get_field(self, sTag):
        """
        Parse the dictionnary and return a dictionnary containing a list
        of numpy corresponding to the tag sTAg.
        """
        
        d_value = {}
        sTag = '//' + sTag
        
        for sDate in list(self.d_roadcast.keys()):
            node = self.d_roadcast[sDate]
            dom_tmp = self.metro_xml_libxml2.create_dom('toto')
            dom_tmp.addChild(node)
            naValue = self.metro_xml_libxml2.get_nodes_in_numpy(dom_tmp, sTag)
            l_measure_date = self.metro_xml_libxml2.\
                         get_nodes_in_list(dom_tmp, '//roadcast-time')
            rlMeasureDate = r.strptime(l_measure_date, "%Y-%m-%dT%H:%M%Z")
            d_value[sDate] = [naValue, rlMeasureDate]

        return d_value

    def plot_all(self, sPngFilename):
        """
        Generates all the plot available.
        """
        
        # Remove the png or xml extension
        if sPngFilename.find('.png') != -1 or\
           sPngFilename.find('.xml') != -1 :
            sFileNoExtension = sPngFilename[:sPngFilename.rfind('.')]
        else:
            sFileNoExtension = sPngFilename
        self.plot_surface_temperature(sFileNoExtension + '_st.png')
        self.plot_road_condition(sFileNoExtension + '_rc.png')
        self.plot_accumulation(sFileNoExtension + '_ac.png')
        self.plot_flux(sFileNoExtension + '_fl.png')
        self.plot_wind_speed(sFileNoExtension + '_ws.png')


    def plot_temperature(self, sElement, sFilename=None):
        # Graph strings
        sMain = _("Observation vs. roadcasts\n" + self.dMainTitle[sElement])
        sXLabel = _("Time (UTC)")
        sYLabel = _("Temperature �C")

        if sElement not in list(self.dMainTitle.keys()):
            sMessage = 'Dictionnary does not have element \'%s\', ' % (sElement) +\
                       'Possible values are %s:' % (list(self.dMainTitle.keys()))
            raise self.Vix_MATCH_ERROR(sMessage)
        
        # Fetching
        lXrc = self.__get_roadcast_time()

        nl_obs =  self.metro_xml_libxml2.\
                 get_nodes_in_list(self.domMatch, '//measure/' +sElement)
        na_obs = numpy.array(vix_util.tranform_list_string_in_number(\
            nl_obs))

        d_roadcast = self.__get_field(sElement)
        
        naFrozingPoint = numpy.zeros(len(lXrc))

        # Graphics creation
        r.CairoPNG(sFilename, type='png', w= self.fPngWidthTemp, \
                   h= self.fPngHeightTemp, bg='white', pointsize=9)

        # Fetch the data in the dictionnary
        lna  = zip(*(list(d_roadcast.values())))[0]
        rl_date = zip(*(list(d_roadcast.values())))[1]

        self.compute_ylim([na_obs]+ list(lna),\
                                    5, -40, 50)

        r.par(mar=r.c(5,4,4,5))
        Vix_Graph.plot_vix(self, lXrc, na_obs)
        self.__draw_sun(self.fLatitude, self.fLongitude)
        Vix_Graph.draw_grid(self, 'hours', 5)
        Vix_Graph.set_tick(self, 'left', self.lUsr[2], self.lUsr[3], 5)
        Vix_Graph.set_tick(self, 'right', self.lUsr[2], self.lUsr[3], 5)
        Vix_Graph.set_tick(self, 'bottom', self.rlUsr[0], self.rlUsr[1],\
                           'hours')
        if self.f_y_max - self.f_y_min > 30: 
            fspace = self.lUsr[2] - 2
        else:
            fspace = self.lUsr[2] - 1.5

        Vix_Graph.set_tick(self, 'bottom', self.rlUsr[0], self.rlUsr[1],\
                           'day', fspace)
        Vix_Graph.write_labels(self, sMain, sBottom=sXLabel, sLeft=sYLabel,\
                               dist_left=2.5, dist_bottom=2.8)

        # Set the colors at the first time
        if len(self.d_color_roadcast) == 0:
            i = 0
            for sKey in list(d_roadcast.keys()):    
                self.d_color_roadcast[sKey] = self.lColor[i]
                i = i+1
        # Draw lines of forecast
        for sKey in list(d_roadcast.keys()):    
            r.lines(d_roadcast[sKey][1], \
                     d_roadcast[sKey][0], pch=21, cex=0.750, \
                    col=self.d_color_roadcast[sKey], lwd=2)

        r.points(self.rX, na_obs, pch=19, cex=0.5, col='blue')
        
        sMessage =  _("File %s written") % (sFilename)
        print(sMessage)
        r.graphics_off()

    def plot_wind_speed(self, sFilename):
        """
        Plot the wind speed.
        """
        # Titles
        sMain = _("Observation vs. roadcasts\nWind speed")
        sYLabel = _("Speed (km/h)")
        sXLabel = _("Time (UTC)")
        sColor = 'blue'

        # Fetch data
        lX = self.__get_roadcast_time()
        lX = lX[:-1]
        nlWS = self.metro_xml_libxml2.\
               get_nodes_in_list(self.domMatch, '//measure/ws' +sElement)
        naWS = numpy.array(vix_util.tranform_list_string_in_number(nlWS))

        # Draw graph
        r.CairoPNG(sFilename, w = self.fPngWidthWS, h = self.fPngHeightWS,\
                   bg='white', pointsize=9)
        self.compute_ylim([naWS], 5, 0, 200)
        self.f_y_min = 0
        Vix_Graph.plot_vix(self, lX, naWS)
        self.__draw_sun(self.fLatitude, self.fLongitude, 0)
        Vix_Graph.draw_grid(self, 'hours', 5)
        Vix_Graph.set_tick(self, 'left', self.lUsr[2], self.lUsr[3], 5)
        Vix_Graph.set_tick(self, 'bottom', self.rlUsr[0], self.rlUsr[1],\
                           'hours')
        fspace = self.lUsr[2] - 1
        Vix_Graph.set_tick(self, 'bottom', self.rlUsr[0], self.rlUsr[1],\
                           'day', fspace)
        Vix_Graph.write_labels(self, sMain, sBottom=sXLabel, sLeft=sYLabel,\
                               dist_left=3, dist_bottom=2.8)
        r.lines(self.rX, naWS, lty=1, col=sColor, \
                    lwd=self.nLineWidth)

        sMessage =  _("File %s written") % (sFilename)
        print(sMessage)
        r.graphics_off()

    def compute_ylim(self, lna, nStep, fMin, fMax):
        """
        Name: compute_ylim
        
        Parameters: [[I] lna : list of numpy
                      nStep : distance between 2 horizontal lines
                      fMin : Min for threshold
                      fMax : Max for threshold
        Returns: None
    
        Description: Compute the max and min of all the curves.
    
        Notes: 
        
        Revision History:
        Author		Date		Reason
        Miguel Tremblay      November 24th 2004   
        """
        
        # Compute the min and max for all the value
        self.f_y_min = int(vix_util.vix_min(fMin, lna))-1
        self.f_y_min = self.f_y_min - self.f_y_min%nStep
        if self.f_y_min > 0:
            self.f_y_min = 0
            
        self.f_y_max = int(vix_util.vix_max(fMax, lna)+0.5)
        self.f_y_max = self.f_y_max + (nStep-self.f_y_max%nStep)
        if self.f_y_max > fMax:
            self.f_y_max = fMax

    def __draw_sun(self, fLat, fLon, fFloor=None):
        """
        Draw the yellow triangles representing the sun.
        """

        if fFloor == None:
            fFloor = self.lUsr[2]
        rYdomain = r.c(fFloor, self.lUsr[3], fFloor)
        rdays = self.get_date_list('day')
        for rday in rdays:
            # Transform R object in iso 8601
            rpy.set_default_mode(rpy.BASIC_CONVERSION)
            sDay = r.format(rday, format="%Y-%m-%d")
            rpy.set_default_mode(rpy.NO_CONVERSION)
            (sSunrise, sSunset) = vix_util.get_sunrise_sunset\
                                  (sDay, fLat, fLon)

            rSunrise = r.strptime(sSunrise, "%Y-%m-%dT%H:%M:%S")
            rSunset = r.strptime(sSunset, "%Y-%m-%dT%H:%M:%S")
            # Compute half-way between sunset and sunrise for the top
            # of the triangle displayed on the graph. Use ctime
            sMiday = vix_util.get_half_time(sSunrise, sSunset)
            rMiday = r.strptime(sMiday, "%Y-%m-%dT%H:%M:%S")
            # X-domain of the triangle
            rXdomain = r.c(rSunrise, rMiday, rSunset)
            r.polygon(rXdomain, rYdomain, col='lemonchiffon',  border='NA')

        return
        
    def plot_road_condition(self, sFilename):
        """
        Plot a bar plot showing the road condition.
        Green: dry road 1
        Cyan : wet 2
        Yellow : Mix water/snow 3
        Yellow : Dew 4
        Yellow : Melting snow 5
        Red : Frost 6
        Red : Icing road 7
        Red : Ice/snow 8
        """
        
        lsRoadCondition = [_("Dry"),_("Wet"),
                           _("Ice/snow"),_("Mix water/snow"),
                           _("Dew"),_("Melting snow"),
                           _("Frost/Black ice"),_("Freezing rain"),]
        dColorMap = {1:'green',
                     2:'cyan',
                     3:'orange',
                     4:'orange',
                     5:'orange',
                     6:'red',
                     7:'red',
                     8:'red'}
        sMain = _("Road condition")
        sXLabel = _("Time (UTC)")
        sYLabel = _("Road condition")

        # Get the road condition
        lX = self.__get_roadcast_time()
        nlRC = self.metro_xml_libxml2.\
                  get_nodes_in_list(self.domMatch, '//rc')
        naRC = numpy.array(vix_util.tranform_list_string_in_number(nlRC))
        naOnes = numpy.ones(len(naRC))
        
        # Map rc to color
        lColor = [dColorMap[x] for x in naRC]

        r.CairoPNG(sFilename, w = self.fPngWidthRC, h = self.fPngHeightRC, \
                   bg='white', pointsize=9)
        
        r.par(mar=r.c(5,10,4,2)) # Put more space at the left of Y axis
        self.f_y_min = 0
        self.f_y_max = 9
        Vix_Graph.plot_vix(self, lX, naRC)
        Vix_Graph.write_labels(self, sMain,  sBottom=sXLabel, \
                                dist_bottom=3.3)
        Vix_Graph.draw_grid(self, 'hours', 1)        
        Vix_Graph.set_tick(self, 'bottom', self.rlUsr[0], self.rlUsr[1], \
                           'hours')
        Vix_Graph.set_tick(self, 'bottom', self.rlUsr[0], self.rlUsr[1], \
                           'day', self.lUsr[2]-1.5)
        
        r.axis(2, at=list(range(1,9)), labels=lsRoadCondition)

        r.points(self.rX, naRC, col=lColor, pch=19, cex=0.50)

        print('Image', sFilename, 'written')
        r.dev_off()


    def plot_accumulation(self, sFilename):
        """
        Plot the accumulation in a bar plot and save the
        png in sFilename.
        """
        # Get the road condition
        sTitle = _("Accumulation on the road")
        sYLeftLabel = _("Water (mm)")
        sYRightLabel = _("Snow/Ice (cm) ")
        sXLabel = _("Time (UTC)")
        sIceColor = 'LightSkyBlue'
        sRainColor = 'blue'

        # Fetch data
        lX = self.__get_roadcast_time()
        # Add one hour to lX
        sLastElement = lX[-1][:16]
        fLastElement = time.mktime(time.strptime(\
            sLastElement,"%Y-%m-%dT%H:%M"))
        fLastElement = fLastElement + 3600
        sLastElement =  xml.utils.iso8601.tostring(round(fLastElement))
#        print sLastElement
        lX.append(sLastElement)

        lRCra = self.metro_xml_libxml2.\
                get_nodes_in_list(self.domRoadcast, '//ra')
        npRCra = numpy.array(vix_util.tranform_list_string_in_number(lRCra))
        lRCsn = self.metro_xml_libxml2.\
                get_nodes_in_list(self.domRoadcast, '//sn')
        npRCsn = numpy.array(vix_util.tranform_list_string_in_number(lRCsn))
        lRCqp_sn = self.metro_xml_libxml2.\
                   get_nodes_in_list(self.domRoadcast, '//qp-sn')
        npRCqp_sn  = numpy.array(vix_util.tranform_list_string_in_number(\
            lRCqp_sn))
        lRCqp_ra = self.metro_xml_libxml2.\
                   get_nodes_in_list(self.domRoadcast, '//qp-ra')
        npRCqp_ra = numpy.array(vix_util.tranform_list_string_in_number(\
            lRCqp_ra))
                
        # Subsample snow and rain to be beautiful
        rX = r.strptime(lX, "%Y-%m-%dT%H:%M%Z")
        l_ctime = r.as_POSIXct(rX).\
                  as_py(rpy.BASIC_CONVERSION)
        lSub_ra = []
        lSub_sn = []
        lSub_qp_sn = []
        lSub_qp_ra = []
        lX_sub = []
        i = 0
        acc_sn = 0
        acc_ra = 0
        precip_sn = 0
        precip_ra = 0

        while i < len(l_ctime)-1:
            lX_sub.append(lX[i])
            j=i+1
            acc_sn = naRCsn[i]
            acc_ra = naRCra[i]
            precip_sn = naRCqp_sn[i]
            precip_ra = naRCqp_ra[i]

            # Concat for one hour.
            k = 1
            while l_ctime[j]%3600 != 0:
                acc_sn = acc_sn + naRCsn[j]
                acc_ra = acc_ra + naRCra[j]
                precip_sn = precip_sn + naRCqp_sn[i]
                precip_ra = precip_ra + naRCqp_ra[i]
                j=j+1
                k = k+1
                if j == len(l_ctime):
                    lSub_sn.append(acc_sn/k)
                    lSub_ra.append(acc_ra/k)
                    lSub_qp_sn.append(precip_sn/k)
                    lSub_qp_ra.append(precip_ra/k)
                    break
            lSub_sn.append(acc_sn/k)
            lSub_ra.append(acc_ra/k)
            lSub_qp_sn.append(precip_sn/k)
            lSub_qp_ra.append(precip_ra/k)
                    
            i = j

        r.CairoPNG(sFilename, w = self.fPngWidthPR, h = self.fPngHeightPR, \
                   bg='white', pointsize=9)
        r.par(mar=r.c(5,4,4,5)) # Set margins to have space for right y-axis
        self.f_y_min = 0
        self.f_y_max = vix_util.vix_max(200, [numpy.array(lSub_ra),\
                                        numpy.array(lSub_sn),\
                                              numpy.array(lSub_qp_sn), \
                                         numpy.array(lSub_qp_ra)])+0.1
        if self.f_y_max < 1.0:
            self.f_y_max = 1.0
        lX_sub.append(sLastElement)
        lSub_ra.append(0)
        lSub_sn.append(0)
        Vix_Graph.plot_vix(self, lX_sub, lSub_ra)
        Vix_Graph.set_tick(self, 'bottom', self.rlUsr[0], self.rlUsr[1], \
                           'hours')
        Vix_Graph.set_tick(self, 'bottom', self.rlUsr[0], self.rlUsr[1], \
                           'day', self.lUsr[2]- (self.f_y_max-self.f_y_min)/5)        

        Vix_Graph.write_labels(self, sTitle, sRight=sYRightLabel, \
                               sBottom=sXLabel, dist_bottom=3, \
                               sLeft=sYLeftLabel, dist_right=3, dist_left=3, \
                               sColL=sRainColor, sColR=sIceColor)
        if self.f_y_max <= 1:
            fGridDist = 0.1
        elif self.f_y_max < 2:
            fGridDist = 0.2            
        elif self.f_y_max < 5:
            fGridDist = 0.5
        elif self.f_y_max < 20:
            fGridDist = 1.0
        else:
            fGridDist = self.f_y_max / 10
        Vix_Graph.draw_grid(self, 'hours', fGridDist)
        # Repaint left y-axis
        r.axis(2, col=sRainColor)
        # Add right y-axis
        r.axis(4, col=sIceColor)

        # Columns for ice and rain.
        rlMin = Vix_Graph.get_date_list(self, 'min')
        rlSub = []
        i=j=0
        while i < len(rlMin)-60:
            rStartRain = rlMin[i]
            rStopRain = rlMin[i+30]
            rlSub.append(rStartRain)
            r.rect(rStartRain, 0, rStopRain, lSub_ra[j], col =sRainColor)
            rStartIce = rlMin[i+30]
            rStopIce = rlMin[i+60]
            r.rect(rStartIce, 0, rStopIce, lSub_sn[j], col = sIceColor)
            i += 60
            j= j +1
        # Draw last rectangle
        rLastTime = rlMin[len(rlMin)-1]
        rBeforeLastTime = rlMin[i]
        rMidLastTime = rlMin[len(rlMin)-(len(rlMin)-i)/2-1]
        rlSub.append(rLastTime)
        r.rect(rBeforeLastTime, 0, rMidLastTime, lSub_ra[j], col = sRainColor)
        r.rect(rMidLastTime, 0, rLastTime, lSub_sn[j], col = sIceColor)

        # Put line for precipitation
        if len(rlSub) > len(lSub_qp_sn):
            rlSub = rlSub[:-1]
        # Snow/ice
        i = 1
        while i < len(rlSub):
            j = i-1
            while lSub_qp_sn[i] > 0 and i < len(rlSub):
                i = i +1
            if i-1 != j:
                if i < len(rlSub):
                    i = i+1
                r.lines(rlSub[j:i], lSub_qp_sn[j:i], col='white',\
                        lwd=self.nLineWidth+2)
                r.lines(rlSub[j:i], lSub_qp_sn[j:i], col=sIceColor,\
                        lwd=self.nLineWidth )
            i = i+1
        # Rain
        i = 1
        while i < len(rlSub):
            j = i-1
            while lSub_qp_ra[i] > 0 and i < len(rlSub):
                i = i +1
            if i-1 != j:
                if i < len(rlSub):
                    i = i+1
                r.lines(rlSub[j:i], lSub_qp_ra[j:i], col='white',\
                        lwd=self.nLineWidth+2)
                r.lines(rlSub[j:i], lSub_qp_ra[j:i], col=sRainColor,\
                        lwd=self.nLineWidth)
            i = i+1

        print('Image', sFilename, 'written')
        r.dev_off()


    def plot_flux(self, sFilename):
        """
        Plot the different fluxes of roadcast.
        """

        # Titles
        sMain = _("Flux")
        sYLabel = _("Power (W/m�)")
        sXLabel = _("Time (UTC)")
        dColor = { 'ir' : 'red' ,
                   'sf' : 'orange',
                   'fv' : 'LightSkyBlue',
                   'bb' : 'black',
                   'fa' : 'peachpuff',
                   'fc' : 'limegreen',
                   'fg' : 'gray',
                   'fp' : 'purple'}
        dArray = {}
        lnaFlux = []
        
        # Fetch data
        lX = self.__get_roadcast_time()
        lX = lX[:-1]

        for sElement in list(dColor.keys()):
            lArray =  self.metro_xml_libxml2.\
                     get_nodes_in_list\
                     (self.domRoadcast, '//' + sElement)
            dArray[sElement] = numpy.array(\
                vix_util.tranform_list_string_in_number(lArray))
            lnaFlux.append(dArray[sElement])

        # Draw graph
        r.CairoPNG(sFilename, w = self.fPngWidthFL, h = self.fPngHeightFL, \
                   bg='white', pointsize=9)
        self.compute_ylim(lnaFlux, 100, -500, 1000)

        # Labels
        naDummy = list(dArray.values())[0]
        Vix_Graph.plot_vix(self, lX, naDummy)                    
        self.__draw_sun(self.fLatitude, self.fLongitude, 0)
        Vix_Graph.draw_grid(self, 'hours', 100)

        Vix_Graph.set_tick(self, 'left', self.lUsr[2], self.lUsr[3], 100)
#        Vix_Graph.set_tick(self, 'right', self.lUsr[2], self.lUsr[3], 100)
        Vix_Graph.set_tick(self, 'bottom', self.rlUsr[0], self.rlUsr[1],\
                           'hours')
        fspace = self.lUsr[2] - 100
        Vix_Graph.set_tick(self, 'bottom', self.rlUsr[0], self.rlUsr[1],\
                           'day', fspace)
        Vix_Graph.write_labels(self, sMain, sBottom=sXLabel, sLeft=sYLabel,\
                               dist_left=3, dist_bottom=2.8)

        # Lines
        for sElement in list(dColor.keys()):
            r.lines(self.rX, dArray[sElement], lty=1, col=dColor[sElement], \
                    lwd=self.nLineWidth)
                
        sMessage =  _("File %s written") % (sFilename)
        print(sMessage)
        r.graphics_off()

    def plot_wind_speed(self, sFilename):
        """
        Plot the wind speed.
        """
        # Titles
        sMain = _("Wind speed")
        sYLabel = _("Speed (km/h)")
        sXLabel = _("Time (UTC)")
        sColor = 'blue'

        # Fetch data
        lX = self.__get_roadcast_time()
        lX = lX[:-1]
        lWS = self.metro_xml_libxml2.\
               get_nodes_in_list(self.domRoadcast, '//ws')
        npWS = numpy.array(vix_util.tranform_list_string_in_number(lWS))

        # Draw graph
        r.CairoPNG(sFilename, w = self.fPngWidthWS, \
                   h = self.fPngHeightWS, bg='white', pointsize=9)
        self.compute_ylim([naWS], 5, 0, 200)
        self.f_y_min = 0
        Vix_Graph.plot_vix(self, lX, naWS)
        self.__draw_sun(self.fLatitude, self.fLongitude, 0)
        Vix_Graph.draw_grid(self, 'hours', 5)
        Vix_Graph.set_tick(self, 'left', self.lUsr[2], self.lUsr[3], 5)
        Vix_Graph.set_tick(self, 'bottom', self.rlUsr[0], self.rlUsr[1],\
                           'hours')
        fspace = self.lUsr[2] - 10
        Vix_Graph.set_tick(self, 'bottom', self.rlUsr[0], self.rlUsr[1],\
                           'day', fspace)
        Vix_Graph.write_labels(self, sMain, sBottom=sXLabel, sLeft=sYLabel,\
                               dist_left=3, dist_bottom=2.8)
        r.lines(self.rX, naWS, lty=1, col=sColor, \
                    lwd=self.nLineWidth)

        sMessage =  _("File %s written") % (sFilename)
        print(sMessage)
        r.graphics_off()


    def __get_roadcast_time(self):
        if self.lX == None:
            self.lX =  self.metro_xml_libxml2.\
               get_nodes_in_list(self.domMatch, '//observation-time')

        return self.lX

    def __set_lat_long(self):
        self.fLatitude = float((self.metro_xml_libxml2.\
               get_nodes_in_list(self.domMatch, '//latitude'))[0])
        self.fLongitude= float((self.metro_xml_libxml2.\
               get_nodes_in_list(self.domMatch, '//longitude')[0]))
    

    
    
def main():

    if len(sys.argv) == 2:
        vix_match = Vix_Match(sys.argv[1])
    else:
        print("Usage vix_match.py match.xml\n")
        return

#    vix_match.plot_road_condition('test.png')
    vix_match.plot_temperature('st','tmp.png')
#    vix_rc.plot_accumulation('precip.png')
#    vix_rc.plot_all('/users/dor/afsg/mit/public_html/selftest.png')
    
if __name__ == "__main__":
    main()

