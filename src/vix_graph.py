##!/usr/bin/env python
# # -*- coding:iso-8859-1  -*-
#
# Vix : Visualization of XML
# ViX is Free and is provided by the Government of Canada
# Copyright (C) 2006 Environment Canada
#
#  Documentation: http://documentation.wikia.com/wiki/ViX
#
#
# Code contributed by:
#  Miguel Tremblay - Canadian meteorological center
#
#  $LastChangedDate$
#  $LastChangedRevision$
#
########################################################################
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
#

"""
 Name:		vix_graph.py

 Description: Parent class for graphics plotted with vix
 
 Notes:

 Author: Miguel Tremblay
 Date: January 19th 2005
"""
import os

import rpy2
import rpy2.robjects as robjects
from rpy2.robjects import r
from rpy2.robjects.packages import importr
from rpy2.robjects import FloatVector
base = importr('base')
            
import numpy

import vix_util
from toolbox import metro_xml_libxml2
from toolbox import metro_xml
from toolbox import metro_util

class Vix_Graph:

    metro_xml_libxml2 = None
    lsXValue = ('month', 'hours', 'day', 'week')
    dAxis = { 'bottom': 1,
              'left' : 2,
              'top' : 3,
              'right' : 4 }
    dFormat = { 'hours' : '%Hh' ,
                'day' : '%d %b',
                'months' : '%b'
                }
    rlUsr = None
    lUsr = None
    GRID_COL = 'gray70'
    f_y_min = None
    f_y_max = None
    lx = None
    rX = None

    def __init__(self):

        # initialisation de la librairie XML
        metro_xml.init()
        # Loading rendering library
        package_cairo = importr("Cairo")
        
        # Load the libxml2 module
        self.metro_xml_libxml2 = metro_util.import_name('toolbox',\
                              "metro_xml_libxml2").Metro_xml_libxml2()
        # Set the variable TZ to UTC
        os.environ['TZ'] = 'UTC'

    def plot_vix(self, ls_x, lf_y):
        """
        Creates a graphics ready to contain x and y.
        ls_x is a list of strings in iso 8601 format.
        lf_y is a list of numbers.
        Nothing is drawn.
        """
        self.lx = ls_x
        self.rX = robjects.r.strptime(ls_x, "%Y-%m-%dT%H:%M")
        # If no y-axis limit already set, use the min and max of y-list
        if self.f_y_min == None:
            self.f_y_min = min(lf_y)
        if self.f_y_max == None:
            self.f_y_max = max(lf_y)

        # Convertir self.f_y_min et self.f_y_max en FloatVector R
        ylim_vector = FloatVector([self.f_y_min, self.f_y_max])
        lf_y_vector = FloatVector(lf_y)

        # Utiliser ylim_vector dans l'appel de la fonction R
        r.par(las=1)
        r.plot(self.rX, lf_y_vector, yaxs='i', xaxs='i', ylim=ylim_vector, type='l', xlab='', ylab='', main='', col='blue', xaxt='n', yaxt='n', lty=0)

        self.rlUsr = r.par('usr')
        self.lUsr = self.rlUsr

    def draw_grid(self, s_dx=None, f_dy=None):
        """
        If dx and/or dy is set, put a gray line on the coorresponding
        axes separated by this value.
        """
        # Draw vertical line
        rlTime = self.get_date_list(s_dx)
        if rlTime == None: # Special case, only 1 value
            return
        rYdomain = robjects.r.c(self.rlUsr[2], self.rlUsr[3])
        for rTime in rlTime:
            rXdomain = robjects.r.c(rTime, rTime)
            r.lines(rXdomain, rYdomain, col=self.GRID_COL)
        # Redraw extremities
        r.lines(robjects.r.c(self.rlUsr[0], self.rlUsr[0]), \
                rYdomain, col='black')
        r.lines(robjects.r.c(self.rlUsr[1], self.rlUsr[1]), \
                rYdomain, col='black')
        # Draw horizontal line
        if f_dy != None:
            rXdomain = robjects.r.c(self.rlUsr[0], self.rlUsr[1])
            lInterval = numpy.arange(self.rlUsr[2], self.rlUsr[3], f_dy)
            for i in lInterval[1:]:
                # Convertir chaque valeur 'i' en un vecteur R
                rYdomain = FloatVector([i, i])
                r.lines(rXdomain, rYdomain, col=self.GRID_COL)


    def set_tick(self, sAxis, start, stop, increment, distance=None, \
                 format=None):
        """
        Set the tick on a given axis.
        sAxis can be in ['left', 'right', 'bottom', 'top']
        increment can be ['day', 'months', 'hours']
        distance is the distance of tick from the bottom of the graphic
        format is the format of the text. See help in R for strptime for
         details.
        """
        # Default formatting if none provided
        if format is None and increment in self.dFormat:
            format = self.dFormat[increment]
        
        # Handle vertical axes (left or right)
        if sAxis in ['left', 'right']:
             # Generate tick positions as a range of numbers
            lTickPosition = numpy.arange(start, stop+increment, increment)
             # Convert lTickPosition to FloatVector before sending to R
            lTickPosition_r = FloatVector(lTickPosition)
            r.axis(self.dAxis[sAxis], at=lTickPosition_r)
            
        # Handle horizontal axes (bottom or top)
        elif sAxis in ['bottom', 'top']:
            # Get the list of positions for ticks
            rlTime = self.get_date_list(increment)
            if rlTime == None:
                return
            
            rlPosition = base.as_POSIXct(rlTime)
            # Generate labels from the positions using the provided format
            labels = base.format(rlPosition, format=format)

           # If we are dealing with days, we need to adjust the label positioning
            if increment == 'day':
                # Set a greater distance for day labels
                line_distance = 2  
                day_labels = base.format(rlPosition, format="%d %b")
                for i, day_label in enumerate(day_labels):
                    # Check if it's a new day to avoid duplicate labels
                    if i == 0 or day_label != day_labels[i - 1]:
                        # Write the day label with increased distance from the axis
                        r.mtext(day_label, side=1, line=line_distance, at=rlPosition[i])
            else:
                # For non-day labels, use the standard positioning
                r.axis(self.dAxis[sAxis], at=rlPosition, labels=labels, las=1)

            if len(rlTime) == 2: # If there is only 2 values write them at the
               rlPosition =robjects.r.c(robjects.r.strptime(self.lx[0], format="%Y-%m-%dT%H:%M%z"), \
                            robjects.r.strptime(self.lx[-1], format="%Y-%m-%dT%H:%M%z"))
            rlTime = base.as_POSIXct(rlTime, format="%Y-%m-%d %H:%M:%S")

        else:
            sVIX_GRAPH_ERROR = "Argument \'%s\' is not valid.\n" % (sAxis) +\
                   "Possible values are %s"  % (list(self.dAxis.keys()))
            raise sVIX_GRAPH_ERROR


    def write_labels(self, sMain=rpy2.rinterface.NULL, sBottom=rpy2.rinterface.NULL, sLeft=rpy2.rinterface.NULL, sTop=rpy2.rinterface.NULL,sRight=rpy2.rinterface.NULL, dist_bottom=1, dist_left=1, dist_top=1, dist_right=1, sColR=rpy2.rinterface.NULL, sColB=rpy2.rinterface.NULL, sColL=rpy2.rinterface.NULL, sColT=rpy2.rinterface.NULL, sColMain=rpy2.rinterface.NULL):
        """
        Write the strings as labels on the graphic.
        """

        if sMain != None:
            r.title(main=sMain, col=sColMain)
        if sBottom != None:
            r.mtext(sBottom, side=1, line=dist_bottom, col=sColB) 
        if sLeft != None:
            r.par(las=0)
            r.mtext(sLeft, side=2, line=dist_left, col=sColL)
            r.par(las=1)
        if sTop != None:
            r.mtext(sTop, side=3, line=dist_top, col=sColT)
        if sRight != None:
            r.par(las=0)
            r.mtext(sRight, side=4, line=dist_right, col=sColR)
            r.par(las=1)
                        
    def get_date_list(self, sRange, lX=None):
        """
        Get a list for x-axis increment by the keyword sRange
        sRange can be: years, months, days, hours.
        """
        
        if lX == None:
            lX = self.lx
    
        if len(lX) < 2:
            return None

        if sRange == 'day':# Strip the hour information
            sDay1 = lX[0][0:10]
            sDay2 = lX[-1][0:10]
            rRange = [robjects.r.strptime(sDay1, format="%Y-%m-%d"), \
                      robjects.r.strptime(sDay2, format="%Y-%m-%d")]
        else:
            rRange = [robjects.r.strptime(lX[0], format="%Y-%m-%dT%H:%M"), \
                      robjects.r.strptime(lX[-1], format="%Y-%m-%dT%H:%M")]

        rlTime = robjects.r.seq(rRange[0], rRange[1], by=sRange)

        return rlTime

    ####################################################
    # Name: compute_ylim
    #
    # Parameters: [[I] lnp : list of numpy
    #                  nStep : distance between 2 horizontal lines
    #                  fMin : Min for threshold
    #                  fMax : Max for threshold
    # Returns: None
    #
    # Description: Compute the max and min of all the curves.
    #
    # Notes: 
    #
    # Revision History:
    #  Author		Date		Reason
    # Miguel Tremblay      November 24th 2004   
    #####################################################
    def compute_ylim(self, lnp, nStep, fMin, fMax):
        # Compute the min and max for all the value
        self.f_y_min = int(vix_util.vix_min(fMin, lnp))-1
        self.f_y_min = self.f_y_min - self.f_y_min%nStep

        if self.f_y_min > 0:
            self.f_y_min = 0

        self.f_y_max = int(vix_util.vix_max(fMax, lnp)+0.5)
        self.f_y_max = self.f_y_max + (nStep-self.f_y_max%nStep)
        if self.f_y_max > fMax:
            self.f_y_max = fMax
