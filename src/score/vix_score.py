#!/usr/bin/env python
# # -*- coding:iso-8859-1  -*-
#
# Vix : Visualization of XML
# ViX is Free and is provided by the Government of Canada
# Copyright (C) 2006 Environment Canada
#
#  Documentation: http://documentation.wikia.com/wiki/ViX
#
#
# Code contributed by:
#  Miguel Tremblay - Canadian meteorological center
#
#  $LastChangedDate$
#  $LastChangedRevision$
#
########################################################################
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
#


"""
 Name:		vix_score
 Description: Create the grapics for the scores.
 
 Notes:

 Author: Miguel Tremblay
 Date: July 21st 2005
"""

import sys
import os
import xml.utils.iso8601
import time
import rpy
from rpy import r
sys.path.append('/users/dor/afsg/mit/svn/macadam/vix/src/')

import vix_util
from vix_graph import Vix_Graph

from gettext import gettext as _

class Vix_score(Vix_Graph):

    fPngWidthScore = 640
    fPngHeightScore = 480
    fPngWidthCor = 640
    fPngHeightCor = 300
    fPngWidthReal = 640
    fPngHeightReal = 300

    domScore = None
    sType = ''
    lElement = ['at', 'td', 'st']
    lScore = ['rms', 'bias', 'linear-correlation', 'rms-unbias', \
              'mean-absolute-error']
    dColorElement = { 'at' : 'red',
                      'td' : 'green',
                      'st' : 'blue',
                      'ws' : 'orange'}

    dMinMax = { 'rms' : [0, 7],
                'rms-unbias' :  [0, 5],
                'mean-absolute-error' : [0, 5],
                'bias' : [-5, 5],
                'linear-correlation' : [0, 1.5]
                }
    dImgSize = {'rms' : [fPngWidthScore, fPngHeightScore],
                'bias': [fPngWidthScore, fPngHeightScore],
                'rms-unbias' : [fPngWidthScore, fPngHeightScore],
                'mean-absolute-error' : [fPngWidthScore, fPngHeightScore],
                'linear-correlation' : [fPngWidthCor, fPngHeightCor],
                'realisation' : [fPngWidthReal, fPngHeightReal]}

    lDates = []
    lRealisation = []
    lNodeDates = None
    dScore = {}
    sMonth = ''
    sYear = ''
    fLatitude = None
    fLongitude = None
    bSolarDisplay = None
    sStartDate = ''
    sEndDate = ''
    nBinTime = None
    dImagesPath = {}
    
    def __init__(self, sFileMatch):
        self.lDates = []
        self.lRealisation = []
        self.lNodeDates = None
        self.dScore = {}
        # Init libxml2
        Vix_Graph.__init__(self)
        # Load the DOM of match file
        for sScore in self.lScore:
            self.dScore[sScore] = {}
            for sElement in self.lElement:
                self.dScore[sScore][sElement] = []
        self.__load_dom(sFileMatch)
        
    def close(self):
        self.metro_xml_libxml2.free_dom(self.domScore)

    def __load_dom(self, sPathScore):
        """
        Create the DOM of the xml file.
        Load the date for the scores.
        """
        if os.path.exists(sPathScore):
            self.domScore = self.metro_xml_libxml2.read_dom(sPathScore)
            self.__load_data()
        else:
            sNoFileError =  _("This file does not exist: %s") % (sPathScore)
            raise sNoFileError

    def __load_data(self):
        """
        Extract the dates and the scores for the different weather elements.
        """
        self.sType = self.domScore.xpathEval2('//filetype')[0].get_content()
        # Get lat-lon
        try:
            self.fLatitude = float(self.domScore.xpathEval2('//latitude')[0].\
                                   get_content())
            self.fLongitude = float(self.domScore.xpathEval2('//longitude')[0].\
                                    get_content())
            self.bSolarDisplay = True
        except(IndexError):
            self.bSolarDisplay = False
            print "No latitude or longitude, continue with no solar display"
        # Get dates
        self.sStartDate = self.domScore.xpathEval2('//period/start')[0].\
                          get_content()
        self.sEndDate  = self.domScore.xpathEval2('//period/end')[0].\
                         get_content()
        self.sMonth = self.domScore.xpathEval2('//@month')[0].\
                      get_content()
        self.sYear = self.domScore.xpathEval2('//@year')[0].\
                     get_content()
        # Get the scores
        for sElement in self.lElement:
            sXPathElement = '//' + sElement
            self.dScore[sElement] = {}
            for sScore in self.lScore:
                sXPathScore = sXPathElement + '//' + sScore
                lNodeElementScore = self.domScore.xpathEval2(sXPathScore)
                self.dScore[sElement][sScore] = map(lambda x: \
                                                    round(float(x.get_content()),2),\
                                                    lNodeElementScore)
        # Get the dates
        if 'season' in self.sType:
            self.lX = [node.get_properties().next.next.get_content() \
                       for node in self.domScore.xpathEval2\
                       ('//at//rms[@season]')]
            # Get the number of realisation
            self.lRealisation = [int(node.get_properties().next.get_content()) \
                                for node in self.domScore.xpathEval2\
                                ('//at//rms[@realisation]')]
        elif 'aggregate' in self.sType:
            self.lNodeDates = self.domScore.xpathEval2('//at//rms[@season]')
            self.lNodeDates = self.domScore.xpathEval2('//at//rms[@date]')
            self.lDates = map(lambda x: x.get_properties().get_content(), \
                              self.lNodeDates)
            self.lX = self.lDates
            # Get the number of realisation
            lNodeRealisation = self.domScore.xpathEval2\
                               ('//@realisation')
            self.lRealisation = map(lambda x: int(x.get_content()), \
                                    lNodeRealisation[:len(self.lDates)])


        elif 'hour' in self.sType:
            nHourStart = int(self.domScore.xpathEval2('//hour-start')[0].\
                             get_content()[:-1])
            lNodeHour = self.domScore.xpathEval2('//hour-list//hour[@nbr]')
            lHourRover = map(lambda x: int(x.get_properties().\
                                           get_content()), \
                             lNodeHour)
            # Get the number of realisation
            self.lRealisation = map(lambda x: int(x.get_content()),
                                    self.domScore.xpathEval2('//@realisation'))

            # Insert value where there is no observation (hence no score)
            lHour = range(0, max(lHourRover)+1)
            for i in lHour:
                lHour[i] = lHour[i] + nHourStart
                if i not in lHourRover:
                    self.lRealisation.insert(i-nHourStart, 0)
                    for sElement in self.lElement:
                        for sScore in self.lScore:
                            self.dScore[sElement][sScore].\
                                     insert(i,float('nan'))

            self.lX = map(lambda x: xml.utils.iso8601.tostring\
                          (3600*(x)), lHour)


    def plot_all(self, sPngFilepath):
        """
        Generates all the plot available.
        """

        sIndiceBackSlash = sPngFilepath.rfind('/') + 1
        sDirectory = sPngFilepath[:sIndiceBackSlash]
        
        # Remove the png or xml extension
        if '.png' not in sPngFilepath or \
           '.xml' not in sPngFilepath:
            sFileNoExtension = sPngFilepath[sIndiceBackSlash\
                                            :sPngFilepath.rfind('.')]
        else:
            sFileNoExtension = sPngFilepath[sIndiceBackSlash:]

        sDirectoryReal = sDirectory + 'real/'
        if not os.path.exists(sDirectoryReal):
            print "Creating directory", sDirectoryReal
            os.makedirs(sDirectoryReal)
            
        self.plot_realisation( sDirectoryReal + \
                               sFileNoExtension + '_real.png')
        for sScore in self.lScore:
            sDirectoryScore = sDirectory + sScore + '/'
            if not os.path.exists(sDirectoryScore):
                print "Creating directory", sDirectoryScore
                os.makedirs(sDirectoryScore)
            sFilename = sFileNoExtension + '_' + sScore + '.png'
            self.plot_score(sScore, \
                            sDirectoryScore + sFilename)

    def plot_realisation(self, sFilename):
        """
        Plot the number of realisation.
        """
        sMain = _("Number of couple obs/prog")
        sYLabel = _("Number")
        sPlotType = 'realisation'        

        (sXLabel, sDivisionGrid, sDivisionName, sFormat) = \
                  self.__get_xlabel(self.sType)        
        r.GDD(sFilename, type='png',  w = self.dImgSize[sPlotType][0], \
              h = self.dImgSize[sPlotType][1], bg='white', ps=9)
        self.compute_ylim([self.lRealisation], 10, 0, \
                          max(self.lRealisation)+10)
        self.f_y_min = 0
        self.plot_vix(self.lX, self.lRealisation)
            
        # Set the distance between the vertical tick
        if self.f_y_max > 5000:
            nDivision = 1000
        elif self.f_y_max > 1000:
            nDivision =  500
        elif self.f_y_max > 500:
            nDivision = 100
        elif self.f_y_max > 150:
            nDivision = 25
        elif self.f_y_max == 10:
            nDivision = 1
        else:
            nDivision =  10

        Vix_Graph.draw_grid(self, sDivisionGrid, nDivision)
        r.par(cex_axis=0.7) # Modify the font size
        Vix_Graph.set_tick(self, 'bottom', self.rlUsr[0], self.rlUsr[1],\
                           sDivisionName, format=sFormat)
        Vix_Graph.set_tick(self, 'left', self.lUsr[2], self.lUsr[3], nDivision)
        # Draw the rectangle
        fIncrement = (self.lUsr[1]-self.lUsr[0])/len(self.lX)
        rlTime = self.get_date_list(sDivisionGrid)
        if rlTime == None:
            return
        for i in range(0, len(rlTime)-1):
            if i == len(self.lRealisation):
                break

            r.rect(rlTime[i], 0, rlTime[i+1],\
                   self.lRealisation[i], col = 'darkorange')
        if 'aggregate' in self.sType:
            r.rect(rlTime[-1], 0, self.lUsr[1],\
                   self.lRealisation[-1], col = 'darkorange')
        Vix_Graph.write_labels(self, sMain, sBottom=sXLabel, sLeft=sYLabel,\
                               dist_left=2.5, dist_bottom=2.8)
        
        sMessage =  _("Image %s written") % (sFilename)
        print sMessage
        r.graphics_off()
        self.dImagesPath['realisation'] = sFilename
        return
    
    def plot_score(self, sScore, sFilename, \
                   bCorr=False):
        """
        Plot the different score. Correlation is a special case since
        it goes only from -1 to 1.
        """
        # Graph strings
        sMain=_("Score %s") % (sScore) + \
               ' (' + self.lX[0][11:13] + ':00Z)'
        sYLabel = _("Value")
            
        (sXLabel, sDivisionGrid, sDivisionName, sFormat) = \
                  self.__get_xlabel(self.sType)        
        r.GDD(sFilename, type='png', w = self.dImgSize[sScore][0], \
              h = self.dImgSize[sScore][1], bg='white', ps =9)
        self.f_y_min = self.dMinMax[sScore][0]
        self.f_y_max = self.dMinMax[sScore][1]
        
        Vix_Graph.plot_vix(self, self.lX, self.dScore['at'][sScore])
        r.par(cex_axis=0.7) # Modify the font size
        if self.bSolarDisplay and 'hour' in self.sType:
            self.__draw_sun()
        Vix_Graph.draw_grid(self, sDivisionGrid, 1)
        Vix_Graph.set_tick(self, 'bottom', self.rlUsr[0], self.rlUsr[1],\
                           sDivisionName, format=sFormat)
        Vix_Graph.set_tick(self, 'left', self.lUsr[2], self.lUsr[3], 1.0)
        Vix_Graph.write_labels(self, sMain, sBottom=sXLabel, sLeft=sYLabel,\
                               dist_left=2.5, dist_bottom=2.8)
        # Draw a line a zero
        naZeros = [0]*len(self.lX)
        r.lines(self.rX, naZeros, lty='dashed', lwd=0.5)
        for sElement in self.lElement:
            r.lines(self.rX, self.dScore[sElement][sScore], \
                    col=self.dColorElement[sElement], lwd=2)
            r.points(self.rX, self.dScore[sElement][sScore], \
                    col=self.dColorElement[sElement], pch=20, font=2)

        sMessage =  _("Image %s written") % (sFilename)
#        print sMessage
        r.graphics_off()
        self.dImagesPath[sScore] = sFilename
        return

    def __draw_sun(self, fFloor=None):
        """
        Draw the yellow triangles representing the sun.
        """
        if fFloor == None:
            fFloor = self.lUsr[2]
        rYdomain = r.c(fFloor, self.lUsr[3], fFloor)
        rdays = self.get_date_list('day')
        for rday in rdays:
            # Transform R object in iso 8601
            rpy.set_default_mode(rpy.BASIC_CONVERSION)
            sDay = r.format(rday, format="%Y-%m-%d")
            rpy.set_default_mode(rpy.NO_CONVERSION)
            (sSunrise, sSunset) = vix_util.get_sunrise_sunset\
                                  (sDay, self.fLatitude, self.fLongitude)

            rSunrise = r.strptime(sSunrise, "%Y-%m-%dT%H:%M:%S")
            rSunset = r.strptime(sSunset, "%Y-%m-%dT%H:%M:%S")
            # Compute half-way between sunset and sunrise for the top
            # of the triangle displayed on the graph. Use ctime
            sMiday = vix_util.get_half_time(sSunrise, sSunset)
            rMiday = r.strptime(sMiday, "%Y-%m-%dT%H:%M:%S")
            # X-domain of the triangle
            rXdomain = r.c(rSunrise, rMiday, rSunset)
            r.polygon(rXdomain, rYdomain, col='lemonchiffon',  border='NA')

        return


    def __get_xlabel(self, sType):        
        """
        Get the x label and the division type depending of the type
        of the graph.
        """

        # Fetching
        if 'hour' in sType:
            sXLabel = _("Hours UTC (%s %s)")  % (self.sYear, self.sMonth)
            sDivisionGrid = sDivisionName = 'hours'
            sFormat='%H'
        elif 'bin' in sType:
            sXLabel = _("Bin (%s %s)")  % (self.sYear, self.sMonth)
            sDivisionGrid = self.nBinTime
            sDivisionName = 'hours'
            sFormat='%H'            
        elif 'month' in sType:
            sXLabel = _("Date (%s-%s)") % (self.sYear, self.sMonth)
            sDivisionGrid = sDivisionName = 'day'
            sFormat='%d'
        elif 'year' in sType:
            sXLabel = _("Date (%s)") % (self.sYear)
            sDivisionGrid = 'week'
            sDivisionName = 'month'
            sFormat='%b'
        elif 'season' in sType:
            sXLabel = _("Saison pour l'ann�e %s") % (self.sYear)
            sDivisionGrid = 'week'
            sDivisionName = 'month'
            sFormat='%b'
            
        return (sXLabel, sDivisionGrid, sDivisionName, sFormat)

    def get_images_path(self):
        """
        Returns the dictionnary containing the path of all the images
        that have been written.
        """
        return self.dImagesPath
        
def main():

    if len(sys.argv) == 2:
        vix_score = Vix_score(sys.argv[1])
    else:
        print "Usage vix_match_road_obs.py match_road_obs.xml\n"
        return

#    vix_match_road_obs.plot_wind_speed('tmp.png')
    vix_score.plot_realisation('real.png')
#    vix_score.plot_score('st', 'tmp.png')
#    vix_score.plot_score('st', 'tmp2.png', True)
#    vix_match_road_obs.plot_all('tmp.png')
    
if __name__ == "__main__":
    main()

