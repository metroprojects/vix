#!/usr/bin/env python
# # -*- coding:iso-8859-1  -*-
#
# Vix : Visualization of XML
# ViX is Free and is provided by the Government of Canada
# Copyright (C) 2006 Environment Canada
#
##  Documentation: http://documentation.wikia.com/wiki/ViX
#
#
# Code contributed by:
#  Miguel Tremblay - Canadian meteorological center
#
#  $LastChangedDate$
#  $LastChangedRevision$
#
########################################################################
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
#


"""
 Name:		vix_bare_plot
 Description: Plot two elements of an xml file.
 
 Notes: Can plot up to 7 values on the y-axis. Add colors in lCol if you want
  to enhance this number.

 Author: Miguel Tremblay
 Date: December 3rd 2004
"""

import sys

import rpy
from rpy import r
from xml.dom.ext.reader import PyExpat
from xml.xpath import Evaluate

from toolbox import metro_xml_libxml2
from toolbox import metro_xml
from toolbox import metro_util
import vix_util

#######################################
# input  string sFilename : filename of the xml file
#        string sTagX : Element to use for X value
#        strng sTagY1 : Element to use for Y value
#
#####################################

# Define the color of the plot
lCol = ['blue', 'limegreen', 'red', 'gold', 'black', 'cyan', 'olivedrab', 'pink', 'orange']

def main():

    if len(sys.argv) < 4:
        print("Usage vix_bare_plot.py filname.xml tagX tagY")
        return

    # initialisation de la librairie XML
    metro_xml.init()

    # Open xml file
    rpy.set_default_mode(rpy.NO_CONVERSION)
    metro_xml_libxml2 =metro_util.import_name('toolbox',\
                                              "metro_xml_libxml2").Metro_xml_libxml2()
    reader = PyExpat.Reader()    
    dom = metro_xml_libxml2.read_dom(sys.argv[1])

    # X value    
    sTagX = "//" + sys.argv[2] 
    nodes = metro_xml_libxml2.xpath(dom, sTagX)
    lXvalue = []
    for node in nodes:
        lXvalue.append(metro_xml_libxml2.get_node_value(node))

    # Y value
    nNbrYValue = len(sys.argv) - 3
    llYValue = []
    for i in range(0, nNbrYValue):
        sTagY =  "//" + sys.argv[i+3]
        nodes = metro_xml_libxml2.xpath(dom, sTagY)
        lYvalue = []
        for node in nodes:
            lYvalue.append(float(metro_xml_libxml2.get_node_value(node)))
        # Convert list to number
        llYValue.append(lYvalue)
    # Get min and max
    fYGraphMin = vix_util.vix_min(-1000000, llYValue)
    fYGraphMax = vix_util.vix_max(100000, llYValue)

    # check if the X axis is a date.
    if sTagX.find("time") > 0:
        print("X axis is a date format")
        rX_axis = r.strptime(lXvalue, "%Y-%m-%dT%H:%M%Z")
        rAxisExtremity = r.range(rX_axis)
        rDateLabels = r.as_character(r.format(rAxisExtremity,"%d %b"))
    else:
        rX_axis = lXvalue

    # Configure plot
    r.par(las=1 ) # Axis values always horizontals
    r.plot(rX_axis, llYValue[0], xlab=sys.argv[2], ylab=sys.argv[3], col=lCol[0],\
           ylim=r.c(fYGraphMin, fYGraphMax))

    # Print the date correctly
    if sTagX.find("time") > 0:
        lUsr = r.par('usr')
        fDayAxisPosition = fYGraphMin - (fYGraphMax - fYGraphMin)/10
        r.axis(1,  at=r.c(lUsr[0], lUsr[1]), pos=fDayAxisPosition, \
               tick=0, labels=rDateLabels)
    # Vertical axis
    # Il reste a afficher les ticks de la bonne couleur.
    for i in range(0, nNbrYValue):
        print("Value",sys.argv[i+3], 'is of color', lCol[i])
        r.par(new=1, fg=lCol[i])
        r.lines(rX_axis, llYValue[i], col=lCol[i])
    # Add the other 
    input('Please press return to continue...\n')
#    for sElement in l_st_element:
#        print "Element", sElement.nodeValue
    
if __name__ == "__main__":
    main()

