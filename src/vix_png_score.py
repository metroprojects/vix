#!/usr/bin/env python
# # -*- coding:iso-8859-1  -*-
#
# Vix : Visualization of XML
# ViX is Free and is provided by the Government of Canada
# Copyright (C) 2006 Environment Canada
#
#  Documentation: http://documentation.wikia.com/wiki/ViX
#
#
# Code contributed by:
#  Miguel Tremblay - Canadian meteorological center
#
#  $LastChangedDate$
#  $LastChangedRevision$
#
########################################################################
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
#

"""
Name:	       vix_png_score.py
Description: Generate the png graphics for all the different scores
 
Notes: 

Author: Miguel Tremblay
Date: July 25th 2005
"""
import sys
import os
sys.path.append('/users/dor/afsg/mit/svn/macadam/vix/src/ripp4x/')
sys.path.append('/users/dor/afsg/mit/svn/macadam/vix/src/score/')

import vix_score
from vix_ripp4x_score import Vix_ripp4x_score
#from xgen_roadcast import Xgen_roadcast
from vix_png import Vix_png

class Vix_png_score(Vix_png):

    sKeyword = 'rover'
    sType = 'rover/'
    dSubType = {'aggregate-month' : 'score/aggregation/month/',
             'aggregate-year' :'score/aggregation/year/',
             'aggregate-season' :'score/aggregation/season/',
             'month-bin' : 'score/month-bin/',
             'year-bin' : 'score/year-bin/',
             'month-hour' : 'score/month-hour/',
             'year-hour': 'score/year-hour/'}
    nStartDay = 0
    dImagesPath = None

    def __init__(self, lArgs):
        sCall = lArgs[0]
        self.__set_subType(sCall)
        Vix_png.__init__(self, lArgs[1:])
                         
    def __set_subType(self, sCall):
        """
        Set the sub type for the score.
        """
        if sCall not in list(self.dSubType.keys()):
            print("First argument must be in the list %s" % list(self.dSubType.\
                  keys()))
            print("%s is not in this list" % (sCall))
        else:
            self.sSubType = self.dSubType[sCall]

    def process(self):
        """
        Produces the images and ripp4x file for the scores.
        """

        # Get all the filename in the station directory
        lDataDirectory = os.listdir(self.sDataDirectory)
        for sStation in lDataDirectory:
            # Get all the files for this station
            if not os.path.isdir(self.sDataDirectory+sStation):
                continue
            if self.sStationArg != '' and sStation != self.sStationArg :
                continue
            sDataStationDir = self.sDataDirectory+sStation
            for sYear in os.listdir(sDataStationDir):
                if self.sYearArg != '' and sYear != self.sYearArg:
                    continue
                sDataYearDirectory = sDataStationDir + '/' + sYear
                # Year type
                if 'year' in self.sSubType or 'season' in self.sSubType: 
                    sPngDirectory = self.sPngDirectory + sStation +\
                                   '/' + sYear + '/'
                    sStationYear = sStation + '/' + \
                                        sYear + '/' + '/'
                    sRipp4xDirectory = self.sRipp4xDirectory + \
                                              self.sSubType+ \
                                              sStationYear
                    for sDirectory in [sPngDirectory, sRipp4xDirectory]:
                        if not os.path.exists(sDirectory):
                            print("[!!] Creating directory:", sDirectory)
                            os.makedirs(sDirectory)
                    for sFile in os.listdir(sDataYearDirectory):
                        nPositionT = sFile.rfind("T")
                        sHour = sFile[nPositionT:nPositionT+3]
                        sFilename =  sStation + '.' + \
                                    self.sSubType[:-1].replace('/','.') + '.' +\
                                    sYear + '.' + sHour
                        sPngFilename = sFilename + '.png'
                        sRoverPath = sDataYearDirectory+'/'+ sFile
                        sRipp4xFilename = sFilename + '.ripp4x.xml'
                        sRipp4xPath = sRipp4xDirectory + sRipp4xFilename
                        self.process_vix(sDataYearDirectory+'/'+sFile,\
                                         sPngDirectory+sPngFilename)
                        self.process_ripp4x(sRoverPath, \
                                            sRipp4xPath, \
                                            self.sSubType)
                        
                    continue
                for sMonth in os.listdir(sDataYearDirectory):
                    if self.sMonthArg != '' and sMonth != self.sMonthArg:
                        continue
                    # Create the data directories
                    sDataYearDirectory = sDataStationDir + '/' + sYear
                    sStationYearMonth = sStation + '/' +\
                                        sYear + '/' + sMonth+ '/'
                    sRipp4xDirectory = self.sRipp4xDirectory + \
                                              self.sSubType+ \
                                              sStationYearMonth
                    sPngDirectory = self.sPngDirectory + sStation +\
                                    '/' + sYear + '/'+ sMonth + '/'
                    if 'month' in self.sSubType: 
                        sDataYearMonthDirectory = sDataYearDirectory+'/'+sMonth
                        for sFile in os.listdir(sDataYearMonthDirectory):
                            nPositionT = sFile.rfind("T")
                            sHour = sFile[nPositionT:nPositionT+3]
                            sFilename =  sStation + '.' +\
                                        self.sSubType[:-1].replace('/','.') + \
                                        '.'+ sYear +  '.' + sMonth + sHour
                            sPngFilename = sFilename + '.png'
                            sRipp4xFilename = sFilename + '.ripp4x.xml'
                            sRoverPath = sDataYearMonthDirectory+'/'+ sFile
                            sRipp4xPath = sRipp4xDirectory + sRipp4xFilename
                            for sDirectory in [sRipp4xDirectory, sPngDirectory]:
                                if not os.path.exists(sDirectory):
                                    print("Creating directory:", sDirectory)
                                    os.makedirs(sDirectory)
                            self.process_vix(sRoverPath,\
                                             sPngDirectory+sPngFilename)
                            self.process_ripp4x(sRoverPath, \
                                                sRipp4xPath, \
                                                self.sSubType)
                            continue




            
    def process_vix(self, sScorePath, sPngFilename):
        print(sScorePath)
        vix_s = vix_score.Vix_score(sScorePath)
        vix_s.plot_all(sPngFilename)
        self.dImagesPath = vix_s.get_images_path()
#        self.nNbrOfRoadcast = vix_s.get_nbr_roadcast()
        vix_s.close()
        
    def process_ripp4x(self, sRoverPath, sRipp4xFilename, sSubType):
        """
        Create the ripp4x file.
        Note: the sSubType string is of type 'score/whatever/' and we are only
         interested by the string 'whatever' (whatever in [month-hour,
         year-hour, etc.]
        """
        sType = sSubType[sSubType.find('/')+1:sSubType.rfind('/')]
        vix_ripp4x_score = Vix_ripp4x_score(sRipp4xFilename, sType)
        vix_ripp4x_score.process(sRoverPath)
        for sScore in list(self.dImagesPath.keys()):
            vix_ripp4x_score.set_path(sScore, \
                                      self.dImagesPath[sScore])
        vix_ripp4x_score.write_xml_file()

    def process_xgen(self, sRipp4xFilename, sSrcFilename):
        """
        A etre implementé
        """
#        xgen = Xgen_roadcast(sRipp4xFilename)
#        xgen.process(sSrcFilename)
    

if __name__ == "__main__":
    import sys

    lArgs = ['']*5

    if len(sys.argv) < 2:
        print("Usage vix_png_score.py ['aggregate-month', 'month-bin', 'aggregate-year', 'year-hour', 'month-hour', 'year-bin'] station year month")
        sys.exit(1)

    if sys.argv[2] == 'all':
        lStation = ['oav', 'ocy', 'oce', 'ocv', 'obw', 'oby']
    else:
        lStation = [sys.argv[2]]

    for sStation in lStation:
        for i in range(1, len(sys.argv)):
            lArgs[i-1] = sys.argv[i]
            lArgs[1] = sStation
        vix = Vix_png_score(lArgs)
        vix.process()




