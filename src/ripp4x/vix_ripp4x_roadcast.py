#!/usr/bin/env python
## -*- coding:iso-8859-1  -*-
#
# Vix : Visualization of XML
# ViX is Free and is provided by the Government of Canada
# Copyright (C) 2006 Environment Canada
#
#  Documentation: http://documentation.wikia.com/wiki/ViX
#
#
# Code contributed by:
#  Miguel Tremblay - Canadian meteorological center
#
#  $LastChangedDate$
#  $LastChangedRevision$
#
########################################################################
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
#

"""
Name:	       vix_ripp4x_roadcast.png
Description: Class to write the xml file containing data
 about the png images created by vix for a roadcast.
 
Notes: 

Author: Miguel Tremblay
Date: January 6th 2005
"""

import os
import sys
sys.path.append('/users/dor/afsg/mit/svn/macadam/rover/src/')

from vix_ripp4x import Vix_ripp4x
from rover_roadcast import Rover_roadcast

class Vix_ripp4x_roadcast(Vix_ripp4x):

    # These variable are read-only and are not part of the class
    #  in order to be retrieve to perform a search using xpath.
    # Header
    rov_road = None
    sStnName = 'road-station'
    sLatitude = 'latitude'
    sLongitude = 'longitude'
    s_first_rc_date = 'roadcast-time'
    # For files dictionary
    sRoadcastPathNode = 'roadcast-path'
    sForecastPathNode = 'output-forecast'
    sObservationPathNode = 'input-observation'
    sImagesPathNode = 'images-path'
    sSTNode = 'surface-temperature'
    sRCNode = 'road-condition'
    sSANode = 'surface-accumulation'
    sFLNode = 'flux'
    sWSNode = 'wind-speed'

    sFiletype = 'roadcast'
    nodeFilesPath = None
    fVersion = 1.0
    sObservationPath = '/data/cmdn/afsgmit/html/observations/12h/'
    Vix_RIPP4X_ROADCAST_ERROR = ''

    dPath = { sRoadcastPathNode : '',
              sForecastPathNode : '',
              sObservationPathNode : '',
              sSTNode : '' ,
              sRCNode : '',
              sSANode : '',
              sFLNode : '',
              sWSNode : ''}

    def __init__(self, sFilepath):
        Vix_ripp4x.__init__(self, sFilepath, self.fVersion)
    
    def process(self, sRoadcastPath):
        """
        Load the DOM and create the header.
        """
        self.load_dom(sRoadcastPath)
        self.create_header(self.sFiletype)
        # Set the path related to the roadcast
        self.set_path('roadcast-path', sRoadcastPath)
        self.set_path('output-forecast', sRoadcastPath.replace('.roadcast.',\
                                                               '.forecast_out.'))
        sObservationPath = self.sObservationPath + self.rov_road.sStation +\
                           '/' + self.rov_road.sYear + '/' +\
                           self.rov_road.sMonth + '/' +\
                           self.rov_road.sStation + '.' +self.rov_road.sYear +\
                           self.rov_road.sMonth + self.rov_road.sDay + '_'
        nIndice = sRoadcastPath.rfind('/')+len('/oby.roadcast.2005-07-04T')
        sObservationPath =  sObservationPath + \
                           sRoadcastPath[nIndice:nIndice+2]+'00.xml'
        self.set_path(self.sObservationPathNode, sObservationPath)
        
    def load_dom(self, sRoadcastPath):
        if not os.path.exists(sRoadcastPath):
            sVIX_RIPP4X_ERROR = "File does not exists: %s" % ((sRoadcastPath))
            raise sVIX_RIPP4X_ERROR
        self.rov_road = Rover_roadcast(sRoadcastPath)
        self.rov_road.extract_information()
        # Create node for path
        self.nodeFilesPath = self.metro_xml_libxml2.\
                             create_node(None, self.sImagesPathNode)
        
    def __del__(self):
        Vix_ripp4x.__del__(self)
        
    def create_header(self, sFiletype):
        """
        Get the latitude, longitude and station name
        """
        # Latitude and longitude
        [node_Lat, node_Lon]  = self.rov_road.get_lat_lon()
        # Station name
        node_Stn_name = self.metro_xml_libxml2.create_text_node\
                        (None, self.sStnName, self.rov_road.sStation)
        node_rc_prod_date = self.metro_xml_libxml2.create_text_node\
                            (None, self.s_first_rc_date, \
                             self.rov_road.sStartTime)
        
        ## Put the latitude and longitude in the header of ripp4x file
        self.metro_xml_libxml2.append_child(self.nodeHeader, \
                                            node_Lat.copyNode(True))
        self.metro_xml_libxml2.append_child(self.nodeHeader, \
                                            node_Lon.copyNode(True))
        self.metro_xml_libxml2.append_child(self.nodeHeader, node_Stn_name)
        self.metro_xml_libxml2.append_child(self.nodeHeader, node_rc_prod_date)


    def write_xml_file(self):
        """
        Add the nodes for the files path.
        """
        # add all the path of the files
        for sNodeName in self.dPath.keys():
            nodePath = self.metro_xml_libxml2.create_text_node\
                       (None, sNodeName,self.dPath[sNodeName])
            self.metro_xml_libxml2.append_child(self.nodeFilesPath, nodePath)

        self.metro_xml_libxml2.append_child(self.nodeRoot, self.nodeFilesPath)
        Vix_ripp4x.write_xml_file(self)


                
def main():
    import sys
    import libxml2

    # Create the DOM from the file
#    domRoadcast = libxml2.parseFile(sys.argv[1]) 
    vix = Vix_ripp4x_roadcast(sys.argv[1])
#    vix.create_header()
    vix.set_path('surface-temperature',\
                 '/users/dor/afsg/mit/svn/macadam/vix/src/roadcast.png')
    vix.set_path('roadcast-path', sys.argv[1])
    vix.write_xml_file()
#    metro_xml_libxml2.free_dom(domRoadcast)

if __name__ == "__main__":
    main()
