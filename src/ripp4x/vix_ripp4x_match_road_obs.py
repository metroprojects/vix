#!/usr/bin/env python
# # -*- coding:iso-8859-1  -*-
#
# Vix : Visualization of XML
# ViX is Free and is provided by the Government of Canada
# Copyright (C) 2006 Environment Canada
#
#  Documentation: http://documentation.wikia.com/wiki/ViX
#
#
# Code contributed by:
#  Miguel Tremblay - Canadian meteorological center
#
#  $LastChangedDate$
#  $LastChangedRevision$
#
########################################################################
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
#

"""
 Name:	       vix_ripp4x_match_road_obs.png
 Description: Class to write the xml file containing data
  about the png images created by vix for a match of observations
  and roadcast
 Notes: 

 Author: Miguel Tremblay
 Date: June 13th 2005
"""

import os

from vix_ripp4x_roadcast import Vix_ripp4x_roadcast
import vix_ripp4x_roadcast


class Vix_ripp4x_match_road_obs(Vix_ripp4x_roadcast):
    '''
    These variable are read-only and are not part of the class
    in order to be retrieve to perform a search using xpath.
    '''
    # For files dictionary
    sRoverPathNode = 'rover-path'
    sObservationPathNode = 'observation-list'

    # Header
    ls_Header = ['road-station', 'latitude', 'longitude', 'roadcast-time',\
                 'roadcast-path', 'observation-list']

    nodeFilesPath = None
    fVersion = 1.0
    Vix_RIPP4X_MATCH_ERROR = ''
    domRover = None
    sRoverPath = None
    sFiletype = 'rover-match-obs'

    dPath = { Vix_ripp4x_roadcast.sSTNode : '',
              Vix_ripp4x_roadcast.sRCNode : '',
              Vix_ripp4x_roadcast.sWSNode : '',
              Vix_ripp4x_roadcast.sSANode : ''}


    def __init__(self, sFilepath):
        Vix_ripp4x_roadcast.__init__(self, sFilepath)


    def process(self, sRoVerPath):
#        print sRoVerPath
        self.sRoverPath = sRoVerPath
        if not os.path.exists(sRoVerPath):
            sVIX_RIPP4X_ERROR = "File does not exists: %s" % ((sRoVerPath))
            raise sVIX_RIPP4X_ERROR
        self.domRover =  self.metro_xml_libxml2.read_dom(sRoVerPath)
        # Create node for path
        self.nodeFilesPath = self.metro_xml_libxml2.\
                             create_node(None, Vix_ripp4x_roadcast.sImagesPathNode)

        self.create_header(self.sFiletype)

    def __del__(self):
        Vix_ripp4x_roadcast.__del__(self)
        if self.domRover != None:
            self.metro_xml_libxml2.free_dom(self.domRover)
        

    def create_header(self, sFiletype):
        """
        Get the latitude, longitude and station name
        """
        for sTag in self.ls_Header:
            node = self.metro_xml_libxml2.all_matching_xpath\
                   (self.domRover, sTag)
            self.metro_xml_libxml2.append_child(self.nodeHeader, node)

        # Special case for the path of the rover file
        nodePath = self.metro_xml_libxml2.create_text_node\
                   (None, self.sRoverPathNode, self.sRoverPath)
        self.metro_xml_libxml2.append_child(self.nodeHeader, nodePath)

    def set_path(self, sNode, sPath):
        """
        Write path for files.
        """
        if not self.dPath.has_key(sNode):
            sMessage = 'Dictionnary does not have entry \'%s\', ' % (sNode) +\
                       'Possible values are %s:' % (self.dPath.keys())
            raise self.Vix_RIPP4X_MATCH_ERROR, sMessage
        self.dPath[sNode] = sPath

    def write_xml_file(self):
        """
        Add the nodes for the files path.
        """
        Vix_ripp4x_roadcast.write_xml_file(self)


                
def main():
    import sys
    import libxml2

    # Create the DOM from the file
#    domRoadcast = libxml2.parseFile(sys.argv[1]) 
    vix = Vix_ripp4x_match_road_obs('test.xml', sys.argv[1])
#    vix.create_header()
    vix.set_path('surface-temperature',\
                 '/users/dor/afsg/mit/public_html/png/rover/match_road_obs/oav/2004/10/oav.match_road_obs.2004-10-22T16:00Z.rover_st.png')
#    vix.set_path('rover-path', sys.argv[1])
    vix.write_xml_file()
#    metro_xml_libxml2.free_dom(domRoadcast)

if __name__ == "__main__":
    main()
