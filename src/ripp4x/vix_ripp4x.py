#!/usr/bin/env python
# # -*- coding:iso-8859-1  -*-
#
# Vix : Visualization of XML
# ViX is Free and is provided by the Government of Canada
# Copyright (C) 2006 Environment Canada
#
#  Documentation: http://documentation.wikia.com/wiki/ViX
#
#
# Code contributed by:
#  Miguel Tremblay - Canadian meteorological center
#
#  $LastChangedDate$
#  $LastChangedRevision$
#
########################################################################
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
#

"""
Name:	       vix_ripp4x.png
Description: Abstract class to write the xml file containing pertinent data
 about the png images created by vix.
 
Notes: RIPP4X stands for Roadcast Information and Png Paths 4 XHTML. Only one 
 DOM for one object.
 
Author: Miguel Tremblay
Date: January 5th 2005
"""

from toolbox import metro_date
from toolbox import metro_xml_libxml2
from toolbox import metro_xml
from toolbox import metro_util

sRootNode = 'ripp4x'
sHeaderNode = 'header'
sProductionDateNode = 'production-date'
sFiletypeNode = 'filetype'
sVersionNode = 'version'

class Vix_ripp4x:

    # Attribute
    sFilepath = None
    file_ripp4x = None
    fVersion = None
    sFiletype = None
    
    metro_xml_libxml2 = None
    domRipp4x = None
    nodeHeader = None
    nodeRoot = None
    dPath = {}
    
    def __init__(self, sFilepath, fVersion):
        if self.__class__ is Vix_ripp4x:
            sMessage = "class %s is a virtual class" % \
                       str(self.__class__)
            raise NotImplementedError(sMessage)

        self.sFilepath = sFilepath
        self.fVersion = fVersion

        # initialisation de la librairie XML
        metro_xml.init()
        # Load the libxml2 module
        self.metro_xml_libxml2 = metro_util.import_name('toolbox',\
                              "metro_xml_libxml2").Metro_xml_libxml2()

        # Create the DOM
        self.domRipp4x = self.metro_xml_libxml2.create_dom('ripp4x')

        self.nodeRoot = self.metro_xml_libxml2.get_dom_root(self.domRipp4x)

        self.__create_header(self.sFiletype)
        
    def __del__(self):
        self.metro_xml_libxml2.free_dom(self.domRipp4x)

    def __create_header(self, sFiletype):
        """
        Write the common header of each ripp4x file.
        """
        if sFiletype == None:
            sMessage = "Must precise a filetype!"
            raise "VIX_RIPP4X_ERROR", sMessage

        #### Node creation #####
        # header
        self.nodeHeader = self.metro_xml_libxml2.create_node\
                          (None, sHeaderNode)
        # version
        nodeVersion = self.metro_xml_libxml2.create_text_node\
                      (None, sVersionNode, str(self.fVersion))
        # production-date
        sCurrentDateISP8601 = metro_date.get_current_date_iso8601()
        nodeProductionDate =  self.metro_xml_libxml2.create_text_node\
                                  (None, sProductionDateNode, \
                                   sCurrentDateISP8601)
        1# filetype
        nodeFiletype = self.metro_xml_libxml2.create_text_node\
                       (None, sFiletypeNode, sFiletype)

        #### Append nodes ######
        # Put the nodes in the header
        self.metro_xml_libxml2.append_child(self.nodeHeader, nodeVersion)
        self.metro_xml_libxml2.append_child(self.nodeHeader, nodeFiletype)
        self.metro_xml_libxml2.append_child(self.nodeHeader, nodeProductionDate)
        # Put header in root node
        self.metro_xml_libxml2.append_child(self.nodeRoot, self.nodeHeader)

    def write_xml_file(self):
        """
        Write the DOM in a file.
        """
        self.metro_xml_libxml2.write_xml_file(self.domRipp4x, self.sFilepath)

        sMessage = "File %s written" % (self.sFilepath)
        print sMessage

    def set_path(self, sNode, sPath):
        """
        Write path for files.
        """
        
        if not self.dPath.has_key(sNode):
            sMessage = 'Dictionnary does not have entry \'%s\', ' % (sNode) +\
                       'Possible values are %s:' % (self.dPath.keys())
            raise self.Vix_RIPP4X_MATCH_ERROR, sMessage
        self.dPath[sNode] = sPath
    
