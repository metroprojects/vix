#!/usr/bin/env python
# # -*- coding:iso-8859-1  -*-
#
# Vix : Visualization of XML
# ViX is Free and is provided by the Government of Canada
# Copyright (C) 2006 Environment Canada
#
#  Documentation: http://documentation.wikia.com/wiki/ViX
#
#
# Code contributed by:
#  Miguel Tremblay - Canadian meteorological center
#
#  $LastChangedDate$
#  $LastChangedRevision$
#
########################################################################
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
#

"""
Name:	       vix_ripp4x_score.py
Description: Class to write the xml file containing data
 about the png images created by vix for the scores.
 
Notes: 

Author: Miguel Tremblay
Date: October 28th 2005
"""

import os

from vix_ripp4x import Vix_ripp4x

# For files dictionary
sRoverFile = 'rover-filepath'
sBiasNode = 'bias'
sRmsNode = 'rms'
sRmsUnbiasNode = 'rms-unbias'
sMeanAbsoluteErrorNode = 'mean-absolute-error'
sLinearCorrelationNode = 'linear-correlation'
sRealisationNode = 'realisation'

class Vix_ripp4x_score(Vix_ripp4x):
    '''
    These variable are read-only and are not part of the class
    in order to be retrieve to perform a search using xpath.
    '''

    # Header
    ls_Header = ['road-station', 'latitude', 'longitude', 'period']
    sImagesPathNode = 'images-path'

    d_color = None
    nodeFilesPathList = None
    fVersion = 1.0
    Vix_RIPP4X_MATCH_ERROR = ''
    domRover = None

    dPath = { sRoverFile : '',
              sBiasNode : '',
              sRmsNode : '',
              sRmsUnbiasNode : '',
              sMeanAbsoluteErrorNode : '',
              sLinearCorrelationNode : '',
              sRealisationNode : ''}


    def __init__(self, sFilepath, sFiletype):
        Vix_ripp4x.sFiletype = 'score-' + sFiletype
        Vix_ripp4x.__init__(self, sFilepath, self.fVersion)


    def __del__(self):
        Vix_ripp4x.__del__(self)
        self.metro_xml_libxml2.free_dom(self.domRover)

    def load_dom(self, sDOMRover):
        if not os.path.exists(sDOMRover):
            sVIX_RIPP4X_ERROR = "File does not exists: %s" % ((sDOMRover))
            raise sVIX_RIPP4X_ERROR
        self.domRover =  self.metro_xml_libxml2.read_dom(sDOMRover) 
        # Create node for path
        self.nodeFilesPath = self.metro_xml_libxml2.\
                             create_node(None, self.sImagesPathNode)

    def create_header(self):
        """
        Get the latitude, longitude and station name.
        """
        dom_rover_header = self.domRover.xpathEval2('//header')[0]

        for sNode in ['number-of-roadcast', 'latitude', 'longitude', \
                      'period', 'hour-start']:
            lNode = dom_rover_header.xpathEval2('//' + sNode )
            if len(lNode) > 0 :
                node = lNode[0].copyNode(True)
                self.metro_xml_libxml2.append_child(self.nodeHeader, \
                                                    node)

        

    def process(self, sDOMRover, ):
        """
        Load the DOM and create the header.
        """
        self.load_dom(sDOMRover)
        self.create_header()
        # Set the path related to the rover file
        self.set_path('rover-filepath', sDOMRover)


    def write_xml_file(self):
        """
        Add the nodes for the files path.
        """
        
        # add all the path of the images
        for sNodeName in self.dPath.keys():
            nodePath = self.metro_xml_libxml2.create_text_node\
                       (None, sNodeName,self.dPath[sNodeName])
            self.metro_xml_libxml2.append_child(self.nodeFilesPath, nodePath)

        # Put header in root node
        self.metro_xml_libxml2.append_child(self.nodeRoot, self.nodeHeader)
        self.metro_xml_libxml2.append_child(self.nodeRoot, self.nodeFilesPath)
        
        Vix_ripp4x.write_xml_file(self)

