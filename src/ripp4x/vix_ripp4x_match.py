#!/usr/bin/env python
# # -*- coding:iso-8859-1  -*-
#
# Vix : Visualization of XML
# ViX is Free and is provided by the Government of Canada
# Copyright (C) 2006 Environment Canada
#
#  Documentation: http://documentation.wikia.com/wiki/ViX
#
#
# Code contributed by:
#  Miguel Tremblay - Canadian meteorological center
#
#  $LastChangedDate$
#  $LastChangedRevision$
#
########################################################################
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
#

"""
Name:	       vix_ripp4x_match.py
Description: Class to write the xml file containing data
 about the png images created by vix for a match of observations
 and roadcast
Notes: 

Author: Miguel Tremblay
Date: May 4th 2005
"""

import os

from vix_ripp4x import Vix_ripp4x

# For files dictionary
sRoverFile = 'rover-filepath'
sObservationPathNode = 'observation-path'
sRoadcastPathNode = 'roadcast-path-list'
sImagesPathNode = 'images-path'
sSTNode = 'surface-temperature'
sRCNode = 'road-condition'
sSANode = 'surface-accumulation'
sWSNode = 'wind-speed'
sATNode = 'air-temperature'
sTDNode = 'dew-point'

class Vix_ripp4x_match(Vix_ripp4x):
    '''
    These variable are read-only and are not part of the class
    in order to be retrieve to perform a search using xpath.
    '''

    # Header
    ls_Header = ['road-station', 'latitude', 'longitude', \
                 'observation-path','roadcast-path-list']

    d_color = None
    nodeFilesPath = None
    fVersion = 1.0
    Vix_RIPP4X_MATCH_ERROR = ''
    domRover = None

    dPath = { sObservationPathNode : '',
              sRoadcastPathNode : '',
              sSTNode : '',
              sRoverFile : '',
              sRCNode : '',
              sSANode : '',
              sWSNode : '',
              sTDNode : '',
              sATNode : ''}


    def __init__(self, sFilepath, sDOMRover):
        Vix_ripp4x.__init__(self, sFilepath, self.fVersion)
        Vix_ripp4x.sFiletype = 'match'
        if not os.path.exists(sDOMRover):
            sVIX_RIPP4X_ERROR = "File does not exists: %s" % ((sDOMRover))
            raise sVIX_RIPP4X_ERROR
        self.domRover =  self.metro_xml_libxml2.read_dom(sDOMRover) 
        # Create node for path
        self.nodeFilesPath = self.metro_xml_libxml2.\
                             create_node(None, sImagesPathNode)

        self.create_header()

    def __del__(self):
        Vix_ripp4x.__del__(self)
        self.metro_xml_libxml2.free_dom(self.domRover)
        

    def create_header(self):
        """
        Get the latitude, longitude and station name
        """
        
        Vix_ripp4x.create_header(self, self.sFiletype)

        for sTag in self.ls_Header:
            node = self.metro_xml_libxml2.all_matching_xpath\
                   (self.domRover, sTag)
            self.metro_xml_libxml2.append_child(self.nodeHeader, node)


    def __set_color(self):
        """
        Get the node containing the path of roadcast as the text in the element
        and the color of the line in the graph as attribute.
        """
        
        for sDate in self.d_color.keys():
            sXpath = '//roadcast-path[@first-roadcast=\"' + sDate + '\"]'
            node = self.nodeHeader.xpathEval2(sXpath)
            # Create the attribute node
            self.metro_xml_libxml2.set_prop(node[0], 'color',\
                                            self.d_color[sDate])

    def write_xml_file(self):
        """
        Add the nodes for the files path.
        """
        
        # add all the path of the images
        for sNodeName in self.dPath.keys():
            nodePath = self.metro_xml_libxml2.create_text_node\
                       (None, sNodeName,self.dPath[sNodeName])
            self.metro_xml_libxml2.append_child(self.nodeFilesPath, nodePath)

        self.__set_color()

#        self.metro_xml_libxml2.append_child(self.nodeRoot, self.node_colors)
        self.metro_xml_libxml2.append_child(self.nodeRoot, self.nodeFilesPath)
        
        Vix_ripp4x.write_xml_file(self)


                
def main():
    import sys
    import libxml2

    # Create the DOM from the file
#    domRoadcast = libxml2.parseFile(sys.argv[1]) 
    vix = Vix_ripp4x_match('test.xml', sys.argv[1])
#    vix.create_header()
    vix.set_path('surface-temperature',\
                 '/users/dor/afsg/mit/svn/macadam/vix/src/match.png')
    vix.set_path('rover-filepath', sys.argv[1])
    vix.write_xml_file()
#    metro_xml_libxml2.free_dom(domRoadcast)

if __name__ == "__main__":
    main()
