


HelloWorld<-function(){
	cat("Hello world\n")
}

draw<-function(){
  # Observation
  lIso8601_obs = myReadTable("/users/dor/afsg/mit/data/home/dirac/workspace/case4/time_observation.data")
  lst_obs = read.table("/users/dor/afsg/mit/data/home/dirac/workspace/case4/st_observation.data")
  ltd_obs = read.table("/users/dor/afsg/mit/data/home/dirac/workspace/case4/td_observation.data")
  lat_obs = read.table("/users/dor/afsg/mit/data/home/dirac/workspace/case4/at_observation.data")
  lst_obs = lst_obs[1:dim(lst_obs)[1],]
  lat_obs = lat_obs[1:dim(lat_obs)[1],]
  ltd_obs = ltd_obs[1:dim(ltd_obs)[1],]
  lXaxis_obs = strptime(lIso8601_obs, "%Y-%m-%dT%H:%M")
  # Forecast
  lIso8601_r = myReadTable("/users/dor/afsg/mit/data/home/dirac/workspace/case4/time_roadcast.data")
  lst_r = read.table("/users/dor/afsg/mit/data/home/dirac/workspace/case4/st_roadcast.data")
  ltd_r = read.table("/users/dor/afsg/mit/data/home/dirac/workspace/case4/td_roadcast.data")
  lat_r = read.table("/users/dor/afsg/mit/data/home/dirac/workspace/case4/at_roadcast.data")
  lst_r = lst_r[1:dim(lst_r)[1],]
  lat_r = lat_r[1:dim(lat_r)[1],]
  ltd_r = ltd_r[1:dim(ltd_r)[1],]
  lXaxis_r = strptime(lIso8601_r, "%Y-%m-%dT%H:%M")

  nPngWidth = 640
  nPngHeight = 480
  png('toto.R.png', nPngWidth, nPngHeight)
      
  
  # Min and max of Y-axis
  par(las=1) # Axis values always horizontals
  nYMinGraph = floor(min(lst_obs,ltd_obs,lat_obs, lst_r,ltd_r,lat_r))
  nYMinGraph = nYMinGraph - nYMinGraph%%5
  if (nYMinGraph>0){
    nYMinGraph=0
  }
  nYMaxGraph = floor(max(lst_obs,ltd_obs,lat_obs, lst_r,ltd_r,lat_r))+1
  nYMaxGraph = nYMaxGraph + (5-nYMaxGraph%%5)

  # Need a plot...
  nLineWidth = 2
  verticalAxis = seq(nYMinGraph, nYMaxGraph, 5)
  plot(lXaxis_r, lst_r, type='l', ylim=c(nYMinGraph,nYMaxGraph),
       xlab='Time (EST)', ylab='Temperature �C', main='Roadcast'
       ,col='blue', xaxt='n', yaxt='n', lty="dashed",
       yaxs='i', lwd=nLineWidth)# st roadcast

  # Yellow background during the day
  range_Hour <- as.POSIXct(round(range(lXaxis_r), "hours"))
  lHour = seq(range_Hour[1], range_Hour[2], by="hours")
  # Get the sunrise and the sunset
#  print(paste(paste("For the date ", as.character(format(range_Hour[1], "%d%b"))), "sunrise is at 12:18:18 UTC and sunset at 21:57:57 UTC"))
  sunrise = c(strptime("2004-11-17T12:18:18", "%Y-%m-%dT%H:%M"),
    strptime("2004-11-18T12:18:18", "%Y-%m-%dT%H:%M"))
  miday =  c(strptime("2004-11-17T16:45:18", "%Y-%m-%dT%H:%M"),
    strptime("2004-11-18T16:45:18", "%Y-%m-%dT%H:%M"))
  sunset = c(strptime("2004-11-17T21:57:57", "%Y-%m-%dT%H:%M"),
    strptime("2004-11-18T21:57:57", "%Y-%m-%dT%H:%M"))
  # Print a yellow triangle
  lUsr = par("usr")
  yDomain = c(lUsr[3], lUsr[4], lUsr[3])
  for (i in 1:length(sunrise)){
    xDomain = c(sunrise[i],miday[i], sunset[i])
    polygon(xDomain, yDomain, col='lemonchiffon', border='NA')
  }
  # Print a brow line where the observations ends.
  xDomain = c(lXaxis_obs[length(lIso8601_obs)],lXaxis_obs[length(lIso8601_obs)])
  yDomain = c(lUsr[3], lUsr[4])
  lines(xDomain, yDomain, col='Maroon', lty='dashed', lwd=nLineWidth)
  
  ### Grid
  xDomain = c(lUsr[1], lUsr[2])
  for (i in seq(nYMinGraph, nYMaxGraph, 5)){
    if (i == nYMinGraph || i == nYMaxGraph){
      next
    }
    yDomain = c(i,i)
    lines(xDomain, yDomain, col='gray70') # Horizontal lines
  }
  axis.POSIXct(1, lHour, at=lHour, format="%Hh")
  axisExtremity = range(lXaxis_r)
  lDateLabels = as.character(format(axisExtremity,"%d %b"))

  # Place dates at the extremity
  fDayAxisPosition = nYMinGraph- (nYMaxGraph-nYMinGraph)/10
  axis(1,  at= c(lUsr[1], lUsr[2]), pos=fDayAxisPosition, tick=FALSE, labels=lDateLabels)
  # Place date at midnight
  lDay = as.POSIXct(round(axisExtremity, "day"))
  lDateLabels = as.character(format(lDay[1],"%d%b"))
#  print (lDay)  
#  axis.POSIXct(1, at=seq(lDay[1], lDay[2], by="day"), pos=fDayAxisPosition, tick=FALSE)
  axis(1, at=lDay[1], pos=fDayAxisPosition, tick=FALSE,labels=lDateLabels )
  yDomain = c(lUsr[3], lUsr[4])
  i = 0
  for (nHour in lHour){
    xDomain = c(nHour, nHour)
    lines(xDomain, yDomain, col='gray70') # Vertical lines
  }
  ####
  ##
  lines(lXaxis_r, lst_r, col='blue', lty='dashed', lwd=nLineWidth)# st roadcast
  lines(lXaxis_obs, lst_obs, col='blue', lwd=nLineWidth)# st observed
  lines(lXaxis_r, lat_r, col='red', lty="dashed", lwd=nLineWidth)# at roadcast
  lines(lXaxis_obs, lat_obs, col='red', lwd=nLineWidth)# at observed
  lines(lXaxis_r, ltd_r, col='green', lty='dashed', lwd=nLineWidth)# td roadcast
  lines(lXaxis_obs, ltd_obs, col='green', lwd=nLineWidth)# td observed

  
  # Horizontal line at each �C
  # Ticks
#  lTickIndices = seq(floor(lUsr[3]),floor(lUsr[4])+1) 
#  axis(2, at=lTickIndices)
  # Axis on the left of plot
  axis(2, at=c(lUsr[3],verticalAxis))
  axis(4, at=c(lUsr[3],verticalAxis))

  dev.off()
  
  par(ask=TRUE)
  readline("Press <Enter> to continue")

  
}

# Gives the first column of the file in a vector as characters.
myReadTable <- function(file){
  table = read.table(file)
  output = table[1:dim(table)[1],]
  output = as.character(output)

  return (output)
}
