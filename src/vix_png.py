#!/usr/bin/env python
# # -*- coding:iso-8859-1  -*-
#
# Vix : Visualization of XML
# ViX is Free and is provided by the Government of Canada
# Copyright (C) 2006 Environment Canada
#
#  Documentation: http://documentation.wikia.com/wiki/ViX
#
#
# Code contributed by:
#  Miguel Tremblay - Canadian meteorological center
#
#  $LastChangedDate$
#  $LastChangedRevision$
#
########################################################################
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
#

"""
 Name:	       vix_png.py
 Description:  Abstract class for the generation of png.
 
 Notes: 

 Author: Miguel Tremblay
 Date: July 20th 2005
"""

import os
import time
import sys
import rpy
sys.path.append('/users/dor/afsg/mit/svn/macadam/vix/src/ripp4x/')
sys.path.append('/users/dor/afsg/mit/svn/macadam/xhtml_gen/src/')
sys.path.append('/users/dor/afsg/mit/svn/macadam/vix/src/')
        
import vix_util

class Vix_png:

    lElement = ['surface-temperature', 'road-condition',\
                'wind-speed',  'surface-accumulation', 'flux']
    
    dPath =  {}
    
    dImagePath = { 'surface-temperature' : '_st.png',
                   'road-condition': '_rc.png',
                   'wind-speed' : '_ws.png',
                   'surface-accumulation' : '_ac.png',
                   'flux': '_fl.png'}


#    sRoverType = 'match_road_obs/'
    sType = ''
    sSubType = ''  
    sArchiveDirectory = '/data/cmdn/afsgmit/html/'
    sHtmlDirectory = '/users/dor/afsg/mit/public_html/'
    sRipp4xDirectory = sHtmlDirectory + 'ripp4x/' #+ sRoverType
    sDataDirectory = ''
    sPngDirectory = ''
    sSrcDirectory = ''
    # Date
    sStationArg = ''
    sYearArg =  ''
    sMonthArg =  ''
    sDayArg =  ''
    sHour= '' # Used for the scores
    
    def __init__(self, lArgs):
        self.sStationArg = lArgs[0]
        self.sYearArg = lArgs[1]
        self.sMonthArg = lArgs[2]
        self.sDayArg = lArgs[3]

        self.sDataDirectory = self.sArchiveDirectory + self.sType +\
                              self.sSubType
        self.sPngDirectory = self.sHtmlDirectory + 'png/' + self.sType +\
                             self.sSubType
        self.sSrcDirectory = self.sHtmlDirectory + 'src/'  + self.sType +\
                             self.sSubType

    def process(self):
        # Get all the filename in the station directory
        lDataDirectory = os.listdir(self.sDataDirectory)
        for sStation in lDataDirectory:
            # Get all the files for this station
            if not os.path.isdir(self.sDataDirectory+sStation):
                continue
            if self.sStationArg != '' and sStation != self.sStationArg :
                continue
            sDataStationDir = self.sDataDirectory+sStation
            for sYear in os.listdir(sDataStationDir):
                if self.sYearArg != '' and sYear != self.sYearArg:
                    continue
                sDataYearDirectory = sDataStationDir + '/' + sYear
                for sMonth in os.listdir(sDataYearDirectory):
                    if self.sMonthArg != '' and sMonth != self.sMonthArg:
                        continue
                    # Create the data directories
                    sDataYearDirectory = sDataStationDir + '/' + sYear
                    sStationYearMonth = sStation + '/' +\
                                        sYear + '/' + sMonth+ '/'
                    sRipp4xDirectory = self.sRipp4xDirectory + \
                                              self.sSubType+ \
                                              sStationYearMonth
                    sPngDirectory = self.sPngDirectory + sStation +\
                                    '/' + sYear + '/'+ sMonth + '/'

                    lDataFiles = os.listdir(sDataYearDirectory+'/'+sMonth)

                    sSrcStationDirectory = self.sSrcDirectory +\
                                           sStationYearMonth
                    for sPath in [sRipp4xDirectory, sPngDirectory]:
                        if not os.path.exists(sPath):
                            print("[!!] Creating", sPath)
                            os.makedirs(sPath)

                    for sDataFile in lDataFiles:
                        if sDataFile.find(self.sKeyword) < 0: 
                            continue
                        sDay = sDataFile[self.nStartDay:self.nStartDay+2]
                        if self.sDayArg != '' and sDay != self.sDayArg:
                            continue
                        sFilenameBase = sDataFile[:sDataFile.rfind('.')]
                        sDataFilename = self.sDataDirectory +\
                                        sStationYearMonth + sDataFile

                        sPngFilename = sPngDirectory +\
                                       sFilenameBase + '.png'

                        for sElement in self.lElement:
                            self.dPath[sElement] = sPngDirectory +\
                                                   sFilenameBase +\
                                                   self.dImagePath[sElement]
                             
                        sRipp4xFilename = sRipp4xDirectory + \
                                          sFilenameBase + '.ripp4x.xml'
                        sSrcFilename = sSrcStationDirectory + \
                                       sFilenameBase + '.html'

                        if not os.path.exists(sSrcStationDirectory):
                            print("[!!] Creating", sSrcStationDirectory)
                            os.makedirs(sSrcStationDirectory)
                        
                        sDataPath = self.sDataDirectory+sStationYearMonth+\
                                    '/'+sDataFile
            
                        try:                            
                            self.process_vix(sDataPath, sPngFilename)
                            self.process_ripp4x(sDataPath, sRipp4xFilename, \
                                                self.dPath)
                            self.process_xgen(sRipp4xFilename, \
                                              sSrcFilename)
                            print(sStation, sYear, sMonth, sDay, sDataFile[-10:-4])

                        except(vix_util.ERROR_VIX):
                            print(sNoFileError)



