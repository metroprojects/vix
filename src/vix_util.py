#
# Vix : Visualization of XML
# ViX is Free and is provided by the Government of Canada
# Copyright (C) 2006 Environment Canada
#
#  Documentation: http://documentation.wikia.com/wiki/ViX
#
#
# Code contributed by:
#  Miguel Tremblay - Canadian meteorological center
#
#  $LastChangedDate$
#  $LastChangedRevision$
#
########################################################################
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
#

"""
 Name:	 vix_util.py
 Description: Miscelleneous functions for ViX
 Notes:
"""


import time
import os
import math
import W3CDate
import Sun
import datetime

import rpy2.robjects as robjects
from rpy2.robjects.packages import importr
        
import numpy

ERROR_VIX = 'VixError'

####################################################
# Name: vix_min
#
# Parameters: [I] float fThreshold : threshold to be used get_min_treshold
#             [I] list of list *plLists
#
# Returns: fMin
#
# Description: Return the minimal value of a list of list.
#
# Notes: 
#
# Revision History:
#  Author		Date		Reason
# Miguel Tremblay       January 4th 2005   
#####################################################
def vix_min(fThreshold, llLists):

    if type(fThreshold) != type(0.0) and type(fThreshold) != type(0):
        sErrorVixUtil = 'First argument must be of type int or float' +\
                   'Type is %s' %(type(fThreshold))
        raise ERROR_VIX(sErrorVixUtil) 
    # Check if we have only a list of list
    if type(llLists[0]) != type([]) and \
       type(llLists[0]) !=  type(numpy.array([])):
        fMin = get_min_threshold(llLists[0], fThreshold)
    else:
        fMin = get_min_threshold(llLists[0], fThreshold)
        for lList in llLists:
            if lList is None or len(lList) == 0:
                continue
            fCurrentMin = get_min_threshold(lList, fThreshold)
            if fCurrentMin < fMin:
                fMin = fCurrentMin

    return fMin

####################################################
# Name: vix_max
#
# Parameters: [I] float fThreshold : threshold to be used get_min_treshold
#             [I] list of list *plLists
#
# Returns: fMax
#
# Description: Return the maximal value of a list of list.
#
# Notes: 
#
# Revision History:
#  Author		Date		Reason
# Miguel Tremblay       January 4th 2005   
#####################################################
def vix_max(fThreshold, llLists):

    if type(fThreshold) != type(0.0) and type(fThreshold) != type(0):
        sErrorVixUtil = 'First argument must be of type int or float' +\
                        'Type given is %s' % (type(fThreshold))
        raise ERROR_VIX(sErrorVixUtil)

    # Check if we have only a list of list
#    if type(llLists[0]) == type([]):
    if len(llLists) == 1:
        fMax = get_max_threshold(llLists[0], fThreshold)
    else:
        fMax = get_max_threshold(llLists[0], fThreshold)
        for lList in llLists:
            if lList is None or len(lList) == 0:
                continue
            fCurrentmax = get_max_threshold(lList, fThreshold)
            if fCurrentmax > fMax:
                fMax = fCurrentmax
    return fMax


# sDate must conform to ISO 8601. ref. http://www.w3.org/TR/NOTE-datetime
# return float ctime
def iso8601toSeconds(sDate):
    if sDate != None:
        d = W3CDate.W3CDate()
        try:
            d.parse(sDate)
        except TypeError as sError:
            sMessage = "The following error occured when parsing the " +\
                       "ISO 8601 date: %s" % (sError)
            raise Exception(sMessage)
        else:
            fDate = d.getSeconds()
    else:
        sMessage = "The following error occured when parsing a " +\
                   "date:\nNo date string to convert"
        raise Exception(sMessage)
    
    return fDate



# Given a date in the ISO 8601 format, return a tuple of iso 8601 formatted
#  strings with the sunrise and the sunset.
def get_sunrise_sunset(sISO8601, fLat, fLon):

    # In the time library of python 'Z' is not supported as a time zone.
    #  Replace it by UTC
    sISO8601 = sISO8601.replace('Z','UTC')

    # Get the tuple based on this time    
    tuple_time = time.strptime(sISO8601, "%Y-%m-%d")
    nYear = tuple_time[0]
    nMonth = tuple_time[1]
    nDay = tuple_time[2]

    cSun = Sun.Sun()
    (fSunriseTimeUTC, fSunsetTimeUTC) = cSun.sunRiseSet(nYear, nMonth, \
                                                        nDay, fLon, fLat)
    # Create the tuple for sunset and sunrise. Year, month and day are the
    #  same that the input.
    tHourMinSecondSunrise = tranform_decimal_hour_in_minutes(fSunriseTimeUTC)
    tHourMinSecondSunset = tranform_decimal_hour_in_minutes(fSunsetTimeUTC)

    # Create a calendar object for the date of sunset and sunrise
    if tHourMinSecondSunset[0] >= 24:
        # Add one day
        tToday =  datetime.datetime(nYear, nMonth, nDay, tHourMinSecondSunset[0]-24, \
                                    tHourMinSecondSunset[1], tHourMinSecondSunset[2])
        tTomorrow = tToday + datetime.timedelta(1)
        tDaySunset = tTomorrow
    else:
        tDaySunset = datetime.datetime(nYear, nMonth, nDay, tHourMinSecondSunset[0], \
                                       tHourMinSecondSunset[1], tHourMinSecondSunset[2])
    # Create a calendar object for the date of sunset and sunrise
    if tHourMinSecondSunrise[0] < 0:
        # Substract one day
        tToday =  datetime.datetime(nYear, nMonth, nDay, tHourMinSecondSunrise[0]+24, \
                                    tHourMinSecondSunrise[1]+60,\
                                    tHourMinSecondSunrise[2]+60)
        tYesterday = tToday - datetime.timedelta(1)
        tDaySunrise = tYesterday
    else:
        tDaySunrise = datetime.datetime(nYear, nMonth, nDay, tHourMinSecondSunrise[0], \
                                       tHourMinSecondSunrise[1], tHourMinSecondSunrise[2])

    tSunrise = (tDaySunrise.year, tDaySunrise.month, tDaySunrise.day,\
               tDaySunrise.hour, tDaySunrise.minute,\
                tDaySunrise.second,\
                tuple_time[6], tuple_time[7], tuple_time[8])
    tSunset = (tDaySunset.year, tDaySunset.month, tDaySunset.day,\
               tDaySunset.hour, tDaySunset.minute,\
                tDaySunset.second,\
                tuple_time[6], tuple_time[7], tuple_time[8])

    sISOsunset = time.strftime( "%Y-%m-%dT%H:%M:%S", tSunset)
    sISOsunrise = time.strftime( "%Y-%m-%dT%H:%M:%S", tSunrise)

    return (sISOsunrise, sISOsunset)
    


####################################################
# Name: tranform_decimal_hour_in_minutes
#
# Parameters: [[I] float fTimeHour : Time in decimal form. Eg 1.90 for
#                       1h54:00
#
# Returns: [nHour, nMinute, nSecond]
#
# Description: Return an array containing the hour, the minutes and the secondes,
#   respectively.
#
# Notes: 
#
# Revision History:
#  Author		Date		Reason
# Miguel Tremblay       October 29th 2004   
#####################################################
def tranform_decimal_hour_in_minutes(fTimeHour):
    # Extract decimal from integer part
    tModHour = math.modf(fTimeHour)
    nHour = int(tModHour[1])
    fDecimalHour = tModHour[0]
    # Transform decimal in minutes
    fMinute = fDecimalHour*60
    # Again, extract the decimal and the integer part
    tModMinute = math.modf(fMinute)
    nMinute = int(tModMinute[1])
    fDecimalMinute = tModMinute[0]
    # Transform decimal in seconds
    fSecond = fDecimalMinute*60
    # Again, extract the decimal and the integer part
    tModSecond = math.modf(fSecond)
    nSecond = int(tModSecond[1])

    return (nHour, nMinute, nSecond)

def seconds2iso8601( fDate, tz="UTC" ):
    tDate = get_struct_time( fDate, tz)
    return time.strftime("%Y-%m-%dT%H:%M:%SZ",tDate)

def get_struct_time( fTime, sTime_zone="UTC" ):
    sOld_zone = os.environ['TZ']
    rslt = (-1,-1,-1,-1,-1,-1,-1,-1,-1)

    try:
        os.environ['TZ'] = sTime_zone
        time.tzset()
        rslt = time.gmtime(fTime)
    finally:
        os.environ['TZ'] = sOld_zone
        time.tzset()

    return rslt


# Given two iso 8601 dates, return the time that is exactly
#  between those two.
def get_half_time(sIso1, sIso2):
    nIso1 = iso8601toSeconds(sIso1)
    nIso2 = iso8601toSeconds(sIso2)
    if nIso2 > nIso1:
        nMid = nIso1 + (nIso2-nIso1)*0.5
    else:
        nMid = nIso2 + (nIso1-nIso2)*0.5

    sMiday = seconds2iso8601(nMid)
    
    return sMiday


####################################################
# Name: get_max_treshold
#
# Parameters: [[I] list lList : list of numbers
#              [I] float fTreshold : treshold
#
# Returns: fMax
#
# Description: Return a maximal value under the threshold fTreshold
#
# Notes: 
#
# Revision History:
#  Author		Date		Reason
# Miguel Tremblay       January 4th 2005   
#####################################################
def get_max_threshold(lList, fThreshold):
    fMax = None
    
    if lList is None :
        sVixUtilError = 'Empty list'
        raise sVixUtilError

    
    # Transform list in a numpy.
    npList = numpy.array(lList)
    # Replace the values under threshold by the maximum of the array
    fMinToReplace = npList.min()
    npForMax = numpy.where(npList > fThreshold, fMinToReplace, npList)
    return npForMax.max()

####################################################
# Name: get_min_treshold
#
# Parameters: [[I] list lList : list of numbers
#              [I] float fTreshold : treshold
#
# Returns: fMin
#
# Description: Return a maximal value under the threshold fTreshold
#
# Notes: 
#
# Revision History:
#  Author		Date		Reason
# Miguel Tremblay       January 4th 2005   
#####################################################
def get_min_threshold(lList, fThreshold):
    fMin = None

    if lList is None:
        sVixUtilError = 'Empty list'
        raise sVixUtilError

    # Transform list in a numpy.
    npList = numpy.array(lList)
    # Replace the values under threshold by the maximum of the array
    fMaxToReplace = npList.max()
    npForMin = numpy.where(npList < fThreshold, fMaxToReplace, npList)
    return npForMin.min()
    
def tranform_list_string_in_number(lList):
    """
    Transform a list of string representing number in float.
    """
    lResult = [float(x) for x in lList]

    return lResult

####################################################
# Name: convert_time
#
# Parameters: [I] string time_str : String representing time in the format "%Y-%m-%dT%H:%M"
#
# Returns: datetime.datetime
#
# Description: Convert a time string in the specified format to a Python datetime object. 
#              Utilizes R's as_POSIXct function for the conversion.
#
# Notes: The function relies on rpy2 for interfacing with R and its as_POSIXct function. 
#        Ensure rpy2 is properly installed and configured.
#
# Revision History:
#  Author       Date              Reason
# Miguel Tremblay  January 10th, 2024   Adaptation to new rpy2 version without rpy_classic module
#####################################################
def convert_time(time_str):
    r_time = robjects.StrVector([time_str])
    posixct_time = r_base.as_POSIXct(r_time, format="%Y-%m-%dT%H:%M")
    return robjects.conversion.rpy2py(posixct_time)[0]
