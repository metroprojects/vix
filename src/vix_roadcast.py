##!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Vix : Visualization of XML
# ViX is Free and is provided by the Government of Canada
# Copyright (C) 2006 Environment Canada
#
#  Documentation: http://documentation.wikia.com/wiki/ViX
#
#
# Code contributed by:
#  Miguel Tremblay - Canadian meteorological center
#
#  $LastChangedDate$
#  $LastChangedRevision$
#
########################################################################
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
#

"""
 Name:		vix_roadcast
 Description: Display the information of a roadcast
 
 Notes:

 Author: Miguel Tremblay
 Date: December 24th 2004 (yes, I'm working)
"""

import sys
import os
import time
import datetime
import math

# rpy2
import rpy2.robjects as robjects
from rpy2.robjects.packages import importr
from rpy2.robjects import FloatVector
r_base = importr('base')
grdevices = importr('grDevices')
graphics = importr('graphics')

import numpy

import vix_util
from vix_graph import Vix_Graph

from gettext import gettext as _

class Vix_Roadcast(Vix_Graph):


    # Attributs
    nYMinGraph = None
    nYMaxGraph = None
    nLineWidth = 2
    fPngWidthST = 640
    fPngHeightST = 480
    fPngWidthRC = 680
    fPngHeightRC = 350
    fPngWidthPR = 640
    fPngHeightPR = 280
    fPngWidthFL = 580
    fPngHeightFL = 380
    fPngWidthWS = 580
    fPngHeightWS = 380
    domRoadcast = None
    lX = None
    fLatitude = None
    fLongitude = None

    dColorElement = { 'at' : 'red',
                      'td' : 'green',
                      'st' : 'blue',
                      'ws' : 'blue'}
    lsTemperatureElement = ['at', 'st', 'td']

    def __init__(self, sFileRoadcast, bInitXML=False):
        """
        Constructor of the class.  When bInitXML is set to True,
        is only call the constructor of Vix_Graph then exit.
        """
        
        Vix_Graph.__init__(self)
        if bInitXML:
            return
        self.__load_dom(sFileRoadcast)
        self.__set_lat_long()


    def close(self):
        self.metro_xml_libxml2.free_dom(self.domRoadcast)


    def __load_dom(self, sPathRoadcast):
        """
        Create the DOM associated with the observations and 
        the roadcast.
        """
        if os.path.exists(sPathRoadcast):
            self.domRoadcast = self.metro_xml_libxml2.read_dom(sPathRoadcast)
        else:
            sNoFileError =  _("This file does not exist: %s") % (sPathRoadcast)
            raise sNoFileError
            

    def plot_all(self, sPngFilename):
        """
        Generates all the plot available
        """
        # Remove the png or xml extension
        if sPngFilename.find('.png') != -1 or\
           sPngFilename.find('.xml') != -1 :
            sFileNoExtension = sPngFilename[:sPngFilename.rfind('.')]
        else:
            sFileNoExtension = sPngFilename
        self.plot_surface_temperature(sFileNoExtension + '_st.png')
        self.plot_road_condition(sFileNoExtension + '_rc.png')
        self.plot_accumulation(sFileNoExtension + '_ac.png')
        self.plot_flux(sFileNoExtension + '_fl.png')
        self.plot_wind_speed(sFileNoExtension + '_ws.png')

    def plot_surface_temperature(self, sFilename=None, bGraphicsOff=True, \
                                 lOptionnalY = []):
        """
        Plot the surface temperature, the air temperature and the dew point.
        If bGraphicsOff is set to false, it is possible for class that
        inheritate for Vix_Roadcast to change this graph.
        """
        dnpWeatherElement = {}
        dCloudColor = {-1: 'purple',
                       0:'skyblue',
                       1:'gray84',
                       2:'gray72',
                       3:'gray60',
                       4:'gray48',
                       5:'gray36',
                       6:'gray24',
                       7:'gray12',
                       8:'black'}
        # Graph strings
        sMain=_("Roadcast")
        sXLabel = _("Time (UTC)")
        sYLabel = _("Temperature °C")
        
        # Fetching
        lXrc = self.__get_roadcast_time()
        # Standard element
        for sTag in self.lsTemperatureElement:
            lsTemperature = self.metro_xml_libxml2.\
                           get_nodes_in_list(self.domRoadcast, '//' + sTag)
            lfTemperature = vix_util.tranform_list_string_in_number(\
                lsTemperature)
            dnpWeatherElement[sTag] = lfTemperature
        # Clouds
        npClouds = self.metro_xml_libxml2.\
                  get_nodes_in_list(self.domRoadcast, '//cc')
        # Map cc to color
        lCloudColor = [dCloudColor[int(x)] for x in npClouds]
        npFrozingPoint = numpy.zeros(len(lXrc))
        
        grdevices.png(sFilename, width=self.fPngWidthST , height=self.fPngHeightST,\
              bg='white')
        self.compute_ylim(list(dnpWeatherElement.values())+lOptionnalY, 5, -40, 75)


        graphics.par(mar=r_base.c(5,4,4,5))
        Vix_Graph.plot_vix(self, lXrc, npFrozingPoint.tolist())
        npCloudPosition = numpy.zeros(len(lXrc))+self.f_y_max

        self.__draw_sun(self.fLatitude, self.fLongitude)
        Vix_Graph.draw_grid(self, 'hours', 5)
        Vix_Graph.set_tick(self, 'left', self.lUsr[2], self.lUsr[3], 5)
        Vix_Graph.set_tick(self, 'right', self.lUsr[2], self.lUsr[3], 5)
        Vix_Graph.set_tick(self, 'bottom', self.rlUsr[0], self.rlUsr[1],\
                           'hours')
        fspace = self.lUsr[2] - 2
        Vix_Graph.set_tick(self, 'bottom', self.rlUsr[0], self.rlUsr[1],\
                           'day', fspace)
        Vix_Graph.write_labels(self, sMain, sBottom=sXLabel, \
                               sLeft=sYLabel,\
                               dist_left=2.5, dist_bottom=3.2)
        # Lines
        # Freezing point
        graphics.lines(self.rX, npFrozingPoint.tolist(), lty='dashed', lwd=2)
        # Clouds
        ly=[self.f_y_max,self.f_y_max]

        # Draw a line for CC 
        for i in range(0,len(npCloudPosition)-1):
            lx=r_base.strptime(lXrc[i:i+2], "%Y-%m-%dT%H:%M")
            sColor=lCloudColor[i]
            graphics.lines(lx, ly, col=sColor, lwd=10)
        # Draw ST, AT, TD
        for sElement in list(dnpWeatherElement.keys()):
            graphics.lines(self.rX, dnpWeatherElement[sElement], \
                    col=self.dColorElement[sElement], \
                    lwd=self.nLineWidth)

        if bGraphicsOff:
            sMessage =  _("File %s written") % (sFilename)
            grdevices.graphics_off()

    def __draw_sun(self, fLat, fLon, fFloor=None):
        """
        Draw the yellow triangles representing the sun.
        """

        if fFloor == None:
            fFloor = self.lUsr[2]
        rYdomain = r_base.c(fFloor, self.lUsr[3], fFloor)
        rdays = self.get_date_list('day')
    
        for rday in rdays:
            # Transform R object in iso 8601
            sDay = datetime.datetime.fromtimestamp(rday).strftime('%Y-%m-%d')
            (sSunrise, sSunset) = vix_util.get_sunrise_sunset\
                                  (sDay, fLat, fLon)

            rSunrise = r_base.strptime(sSunrise, "%Y-%m-%dT%H:%M:%S")
            rSunset = r_base.strptime(sSunset, "%Y-%m-%dT%H:%M:%S")
            # Compute half-way between sunset and sunrise for the top
            # of the triangle displayed on the graph. Use ctime
            sMiday = vix_util.get_half_time(sSunrise, sSunset)
            rMiday = r_base.strptime(sMiday, "%Y-%m-%dT%H:%M:%S")
            # X-domain of the triangle
            rXdomain = r_base.c(rSunrise, rMiday, rSunset)
            graphics.polygon(rXdomain, rYdomain, col='lemonchiffon',  border='NA')

        return
        

    def plot_road_condition(self, sFilename, bGraphicsOff=True):
        """
        Plot a bar plot showing the road condition.
        Green: dry road 1
        Cyan : wet 2
        Orange : Mix water/snow 3
        Orange : Dew 4
        Orange : Melting snow 5
        Red : Frost 6
        Red : Icing road 7
        Red : Ice/snow 8
        """
        
        lsRoadCondition = [_("Dry"),_("Wet"),
                           _("Ice/snow"),_("Mix water/snow"),
                           _("Dew"),_("Melting snow"),
                           _("Frost/Black ice"),_("Freezing rain"),]
        dColorMap = {1:'green',
                     2:'cyan',
                     3:'orange',
                     4:'orange',
                     5:'orange',
                     6:'red',
                     7:'red',
                     8:'red'}

        sMain = _("Road condition")
        sXLabel = _("Time (UTC)")
        sYLabel = _("Road condition")

        # Get the road condition
        lX = self.__get_roadcast_time()
        lRC = self.metro_xml_libxml2.\
                  get_nodes_in_list(self.domRoadcast, '//rc')
        npRC = numpy.array(vix_util.tranform_list_string_in_number(lRC))
        npOnes = numpy.ones(len(npRC))
        
        # Map rc to color
        lColor = [dColorMap[x] for x in npRC]

        grdevices.png(sFilename, w = self.fPngWidthRC, h = self.fPngHeightRC,\
                   bg='white', pointsize=9)

        graphics.par(mar=r_base.c(5,10,4,2)) # Put more space at the left of Y axis
        self.f_y_min = 0
        self.f_y_max = 9
        Vix_Graph.plot_vix(self, lX, npRC.tolist())
        Vix_Graph.write_labels(self, sMain,  sBottom=sXLabel, \
                                dist_bottom=3.3)
        Vix_Graph.draw_grid(self, 'hours', 1)        
        Vix_Graph.set_tick(self, 'bottom', self.rlUsr[0], self.rlUsr[1], \
                           'hours')
        Vix_Graph.set_tick(self, 'bottom', self.rlUsr[0], self.rlUsr[1], \
                           'day', self.lUsr[2]-0.7)
        
        graphics.axis(2, at=list(range(1,9)), labels=lsRoadCondition)
        graphics.points(self.rX, npRC.tolist(), col=robjects.StrVector(lColor), pch=19, cex=0.50)

        if bGraphicsOff:
            grdevices.dev_off()


    def plot_accumulation(self, sFilename, \
                          bGraphicsOff=True, lPI=None, lObsTime=None):
        """
        Plot the accumulation in a bar plot and save the
        png in sFilename.
        """
        # Get the road condition
        sTitle = _("Accumulation on the road")
        sYLeftLabel = _("Water (mm)")
        sYRightLabel = _("Snow/Ice (cm) ")
        sXLabel = _("Time (UTC)")
        sIceColor = 'LightSkyBlue'
        sRainColor = 'blue'

        # Fetch data
        lX = self.__get_roadcast_time()
        # Add one hour to lX
        sLastElement = lX[-1][:16]
        fLastElement = time.mktime(time.strptime(\
                                                sLastElement,"%Y-%m-%dT%H:%M"))
        fLastElement = fLastElement + 3600
        

        # Transform ctime in tuple
        tDate = time.localtime(fLastElement)
        # Transform tuple in string
        sLastElement = time.strftime('%Y-%m-%dT%H:%MZ', tDate)
        
        lX.append(sLastElement)

        
        lRCra = self.metro_xml_libxml2.\
                get_nodes_in_list(self.domRoadcast, '//ra')
        npRCra = numpy.array(vix_util.tranform_list_string_in_number(lRCra))
        lRCsn = self.metro_xml_libxml2.\
                get_nodes_in_list(self.domRoadcast, '//sn')
        npRCsn = numpy.array(vix_util.tranform_list_string_in_number(lRCsn))
        lRCqp_sn = self.metro_xml_libxml2.\
                   get_nodes_in_list(self.domRoadcast, '//qp-sn')
        npRCqp_sn  = numpy.array(vix_util.tranform_list_string_in_number(\
            lRCqp_sn))
        lRCqp_ra = self.metro_xml_libxml2.\
                   get_nodes_in_list(self.domRoadcast, '//qp-ra')
        npRCqp_ra = numpy.array(vix_util.tranform_list_string_in_number(\
            lRCqp_ra))
                 
        # Subsample snow and rain to be beautiful
        rX = r_base.strptime(lX, "%Y-%m-%dT%H:%M")
        l_ctime = r_base.as_POSIXct(rX)
        lSub_ra = []
        lSub_sn = []
        lSub_qp_sn = []
        lSub_qp_ra = []
        lX_sub = []
        i = 0
        acc_sn = 0
        acc_ra = 0
        precip_sn = 0
        precip_ra = 0

        while i < len(l_ctime)-1:
            lX_sub.append(lX[i])
            j=i+1
            acc_sn = npRCsn[i]
            acc_ra = npRCra[i]
            precip_sn = npRCqp_sn[i]
            precip_ra = npRCqp_ra[i]

            # Concat for one hour.
            k = 1
            while l_ctime[j]%3600 != 0:
                acc_sn = acc_sn + npRCsn[j]
                acc_ra = acc_ra + npRCra[j]
                precip_sn = precip_sn + npRCqp_sn[i]
                precip_ra = precip_ra + npRCqp_ra[i]
                j=j+1
                k = k+1
                if j == len(l_ctime):
                    lSub_sn.append(acc_sn/k)
                    lSub_ra.append(acc_ra/k)
                    lSub_qp_sn.append(precip_sn/k)
                    lSub_qp_ra.append(precip_ra/k)
                    break
            lSub_sn.append(acc_sn/k)
            lSub_ra.append(acc_ra/k)
            lSub_qp_sn.append(precip_sn/k)
            lSub_qp_ra.append(precip_ra/k)
                    
            i = j

        grdevices.png(sFilename, w = self.fPngWidthPR, h = self.fPngHeightPR, \
                   bg='white', pointsize=9)

        graphics.par(mar=r_base.c(5,4,4,5)) # Set margins to have space for right y-axis
        self.f_y_min = 0
        self.f_y_max = vix_util.vix_max(200, [numpy.array(lSub_ra),\
                                        numpy.array(lSub_sn),\
                                              numpy.array(lSub_qp_sn), \
                                         numpy.array(lSub_qp_ra)])+0.1
        if self.f_y_max < 1.0:
            self.f_y_max = 1.0
        lX_sub.append(sLastElement)
        lSub_ra.append(0)
        lSub_sn.append(0)
        Vix_Graph.plot_vix(self, lX_sub, lSub_ra)
        Vix_Graph.set_tick(self, 'bottom', self.rlUsr[0], self.rlUsr[1], \
                           'hours')
        Vix_Graph.set_tick(self, 'bottom', self.rlUsr[0], self.rlUsr[1], \
                           'day', self.lUsr[2]- (self.f_y_max-self.f_y_min)/9)        

        Vix_Graph.write_labels(self, sTitle, sRight=sYRightLabel, \
                               sBottom=sXLabel, dist_bottom=4.0, \
                               sLeft=sYLeftLabel, dist_right=3, dist_left=3, \
                               sColL=sRainColor, sColR=sIceColor)

        # Draw the observations as color of background.
        j = 0
        if not bGraphicsOff and lPI != None and lObsTime != None:
            rYdomain = r_base.c(self.lUsr[2], self.lUsr[3])
            for i in range(0, len(lPI)-1):
                nPrecip = lPI[i]
                if int(nPrecip) != 0:
                    rStart = vix_util.convert_time(lObsTime[i])
                    rEnd = vix_util.convert_time(lObsTime[i+1])
                    rXdomain = r_base.c(rStart, rEnd)
                    graphics.rect(rStart, self.lUsr[2], rEnd, self.lUsr[3],\
                           col='paleturquoise',  border='NA')

                    if j > 20:
                        break
                    j = j +1

        if self.f_y_max <= 1:
            fGridDist = 0.1
        elif self.f_y_max < 2:
            fGridDist = 0.2            
        elif self.f_y_max < 5:
            fGridDist = 0.5
        elif self.f_y_max < 20:
            fGridDist = 1.0
        else:
            fGridDist = self.f_y_max / 10
        Vix_Graph.draw_grid(self, 'hours', fGridDist)
        # Repaint left y-axis
        graphics.axis(2, col=sRainColor)
        # Repaint top axis
        graphics.lines(r_base.c(self.lUsr[0], self.lUsr[1]), r_base.c(self.lUsr[3], \
                                                     self.lUsr[3]), \
                col='black')
        # Add right y-axis
        graphics.axis(4, col=sIceColor)


        # Columns for ice and rain.
        rlMin = Vix_Graph.get_date_list(self, 'min')
        rlSub = []
        i=j=0

        while i < len(rlMin)-60:
            rStartRain = rlMin[i]
            rStopRain = rlMin[i+30]
            rlSub.append(rStartRain)
            rStartRain_vector = FloatVector([rStartRain])
            rStopRain_vector = FloatVector([rStopRain])
            lSub_ra_vector = FloatVector([lSub_ra[j]])
            graphics.rect(rStartRain_vector, FloatVector([0]), rStopRain_vector, lSub_ra_vector, col=sRainColor)
            rStartIce = rlMin[i+30]
            rStopIce = rlMin[i+60]
            # Supposons que rStartIce, rStopIce, et lSub_sn[j] sont des nombres flottants
            rStartIce_vector = FloatVector([rStartIce])
            rStopIce_vector = FloatVector([rStopIce])
            lSub_sn_vector = FloatVector([lSub_sn[j]])

            # Appel de graphics.rect avec des vecteurs R
            graphics.rect(rStartIce_vector, FloatVector([0]), rStopIce_vector, lSub_sn_vector, col=sIceColor)
            i += 60
            j= j +1
        # Draw last rectangle
        rLastTime = rlMin[len(rlMin)-1]
        rBeforeLastTime = rlMin[i]
        rMidLastTime = rlMin[int(len(rlMin)-(len(rlMin)-i)/2-1)]
        rlSub.append(rLastTime)
        graphics.rect(rBeforeLastTime, 0, rMidLastTime, lSub_ra[j], col = sRainColor)
        graphics.rect(rMidLastTime, 0, rLastTime, lSub_sn[j], col = sIceColor)

        # Put line for precipitation
        if len(rlSub) > len(lSub_qp_sn):
            rlSub = rlSub[:-1]
        # Snow/ice
        i = 1
        while i < len(rlSub):
            j = i-1
            while  i < len(rlSub) and lSub_qp_sn[i] > 0:
                i = i +1
            if i-1 != j:
                if i < len(rlSub):
                    i = i+1

                lSub = rlSub
                graphics.lines(lSub[j:i] , lSub_qp_sn[j:i], col='white',\
                        lwd=self.nLineWidth+2)
                graphics.lines(lSub[j:i], lSub_qp_sn[j:i], col=sIceColor,\
                        lwd=self.nLineWidth )
            i = i+1
        # Rain
        i = 1
        while i < len(rlSub):
            j = i-1            
            while  i < len(rlSub) and lSub_qp_ra[i] > 0:
                i = i +1
            if i-1 != j:
                if i < len(rlSub):
                    i = i+1

                lSub = rlSub
                graphics.lines(lSub[j:i] , lSub_qp_ra[j:i], col='white',\
                        lwd=self.nLineWidth+2)
                graphics.lines(lSub[j:i], lSub_qp_ra[j:i], col=sRainColor,\
                        lwd=self.nLineWidth )

            i = i+1

        if bGraphicsOff:
            grdevices.dev_off()


    def plot_flux(self, sFilename):
        """
        Plot the different fluxes of roadcast.
        """

        # Titles
        sMain = _("Flux")
        sYLabel = _("Power (W/m²)")
        sXLabel = _("Time (UTC)")
        dColor = { 'ir' : 'red' ,
                   'sf' : 'orange',
                   'fv' : 'LightSkyBlue',
                   'bb' : 'black',
                   'fa' : 'peachpuff',
                   'fc' : 'limegreen',
                   'fg' : 'gray',
                   'fp' : 'purple'}
        dArray = {}
        lnpFlux = []
        
        # Fetch data
        lX = self.__get_roadcast_time()
        lX = lX[:-1]

        for sElement in list(dColor.keys()):
            lArray =  self.metro_xml_libxml2.\
                     get_nodes_in_list\
                     (self.domRoadcast, '//' + sElement)
            dArray[sElement] = numpy.array(\
                vix_util.tranform_list_string_in_number(lArray))
            lnpFlux.append(dArray[sElement])

        # Draw graph
        grdevices.png(sFilename, w = self.fPngWidthFL, h = self.fPngHeightFL, \
                   bg='white', pointsize=9)

        self.compute_ylim(lnpFlux, 100, -1000, 1000)

        # Labels
        npDummy = list(dArray.values())[0]
        Vix_Graph.plot_vix(self, lX, npDummy.tolist())
        self.__draw_sun(self.fLatitude, self.fLongitude, 0)
        Vix_Graph.draw_grid(self, 'hours', 100)

        Vix_Graph.set_tick(self, 'left', self.lUsr[2], self.lUsr[3], 100)
        Vix_Graph.set_tick(self, 'bottom', self.rlUsr[0], self.rlUsr[1],\
                           'hours')
        fspace = self.lUsr[2] - 50
        Vix_Graph.set_tick(self, 'bottom', self.rlUsr[0], self.rlUsr[1],\
                           'day', fspace)

        Vix_Graph.write_labels(self, sMain, sBottom=sXLabel, sLeft=sYLabel,\
                               dist_left=3, dist_bottom=2.8)

        # Lines
        for sElement in list(dColor.keys()):
            graphics.lines(self.rX, dArray[sElement].tolist(), lty=1, col=dColor[sElement], \
                    lwd=self.nLineWidth)
                
        sMessage =  _("File %s written") % (sFilename)
        grdevices.graphics_off()

    def plot_wind_speed(self, sFilename, bGraphicsOff=True, \
                        lOptionnalY = []):
        """
        Plot the wind speed.
        """
        # Titles
        sMain = _("Wind speed")
        sYLabel = _("Speed (km/h)")
        sXLabel = _("Time (UTC)")
        sColor = 'blue'

        # Fetch data
        lX = self.__get_roadcast_time()
        if bGraphicsOff:
            lX = lX[:-1]
        lWS = self.metro_xml_libxml2.\
               get_nodes_in_list(self.domRoadcast, '//ws')
        npWS = numpy.array(vix_util.tranform_list_string_in_number(lWS))

        # Draw graph
        grdevices.png(sFilename, w = self.fPngWidthWS, h = self.fPngHeightWS,\
              bg='white', pointsize=9)

        self.compute_ylim([npWS.tolist()+lOptionnalY], 5, 0, 200)
        self.f_y_min = 0
        Vix_Graph.plot_vix(self, lX, npWS.tolist())
        self.__draw_sun(self.fLatitude, self.fLongitude, 0)
        Vix_Graph.draw_grid(self, 'hours', 5)
        Vix_Graph.set_tick(self, 'left', self.lUsr[2], self.lUsr[3], 5)
        Vix_Graph.set_tick(self, 'bottom', self.rlUsr[0], self.rlUsr[1],\
                           'hours')
        fspace = self.lUsr[2] - (self.f_y_max-self.f_y_min)/9
        Vix_Graph.set_tick(self, 'bottom', self.rlUsr[0], self.rlUsr[1],\
                           'day', fspace)
        Vix_Graph.write_labels(self, sMain, sBottom=sXLabel, sLeft=sYLabel,\
                               dist_left=3, dist_bottom=2.8)
        graphics.lines(self.rX, npWS.tolist(), lty=1, col=sColor, \
                    lwd=self.nLineWidth)
        if bGraphicsOff:
            sMessage =  _("File %s written") % (sFilename)
            grdevices.graphics_off()


    def __get_roadcast_time(self):
        if self.lX == None:
            self.lX =  self.metro_xml_libxml2.\
               get_nodes_in_list(self.domRoadcast, '//roadcast-time')

        return self.lX

    def __set_lat_long(self):
        """
        Set the latitude and the longitude if they are not already set.
        """
        self.fLatitude = float((self.metro_xml_libxml2.\
               get_nodes_in_list(self.domRoadcast, '//latitude'))[0])
        self.fLongitude= float((self.metro_xml_libxml2.\
               get_nodes_in_list(self.domRoadcast, '//longitude')[0]))
    
    
def main():

    if len(sys.argv) == 3:
        vix_rc = Vix_Roadcast(sys.argv[1])
        sImagePath = sys.argv[2] + '/vix.png'
    else:
        print("Usage vix_roadcast.py roadcast.xml output_image_path \n")
        return

    vix_rc.plot_all(sImagePath)
    
if __name__ == "__main__":
    main()

