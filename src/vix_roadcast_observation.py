#!/usr/bin/env python
# # -*- coding:iso-8859-1  -*-
#
# Vix : Visualization of XML
# ViX is Free and is provided by the Government of Canada
# Copyright (C) 2006 Environment Canada
#
#  Documentation: http://documentation.wikia.com/wiki/ViX
#
#
# Code contributed by:
#  Miguel Tremblay - Canadian meteorological center
#
#  $LastChangedDate$
#  $LastChangedRevision$
#
########################################################################
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
#

"""
 Name:		vix_roadcast_observation
 Description: Display the information of a roadcast and the observations
  of the same station.
 
 Notes:

 Author: Miguel Tremblay
 Date: December 15th 2004
"""

import sys
import os
import time
import math
import rpy
from rpy import r
import numpy
from gettext import gettext as _

from toolbox import metro_xml_libxml2
from toolbox import metro_xml
from toolbox import metro_util
import vix_util
import vix_roadcast

#_ = metro_util.init_translation('vix_roadcast_observation')


class Vix_Roadcast_Observation:


    # Attributs
    nYMinGraph = None
    nYMaxGraph = None
    nLineWidth = 2
    nPngWidthST = 800
    nPngHeightST = 600
    nPngWidthRC = 800
    nPngHeightRC = 300

    metro_xml_libxml2 = None
    domObservation = None
    domRoadcast = None

    def __init__(self, sFileRoadcast, sFileObservation):
        # initialisation de la librairie XML
        metro_xml.init()

        # Load the libxml2 module
        self.metro_xml_libxml2 = metro_util.import_name('toolbox',\
                              "metro_xml_libxml2").Metro_xml_libxml2()
        self.metro_xml_libxml2.start()

        # Set the variable TZ to UTC
        os.environ['TZ'] = 'UTC'

        # Avoid conversion of object returned by r.strptime
        rpy.set_default_mode(rpy.NO_CONVERSION)

        # Load the DOM of the roadcast and of the observation.
        #  This method must be called before any other in this method.
        self.__load_dom(sFileObservation, sFileRoadcast)

    def close(self):
        # Free memory accorded to DOM
        self.metro_xml_libxml2.free_dom(self.domRoadcast)
        self.metro_xml_libxml2.free_dom(self.domObservation)
        self.metro_xml_libxml2.stop()

        
    #######################################################
    # Create the DOM associated with the observations and 
    # the roadcast.
    ########################################################
    def __load_dom(self, sPathObservation, sPathRoadcast):

        if sPathObservation != None:
            if os.path.exists(sPathObservation):
                self.domObservation = self.metro_xml_libxml2.read_dom(sPathObservation)
            else:
                sNoFileError =  _("This file does not exist: %s") % (sPathObservation)
                raise sNoFileError
        if sPathRoadcast != None:
            if os.path.exists(sPathRoadcast):
                self.domRoadcast = self.metro_xml_libxml2.read_dom(sPathRoadcast)
            else:
                sNoFileError =  _("This file does not exist: %s") % (sPathRoadcast)
                raise sNoFileError
            


    def plot_roadcast_and_observation(self, sFilename=None):

        if self.domRoadcast == None:
            sVixNoDomError = "No DOM for roadcast. Please use" +\
                             'start(sFilepathroadcast, sFilepathObservation) '\
                             +'before any call to vix_roadcast_observation'
            raise sVixNoDomError
        elif self.domObservation == None:
            sVixNoDomError = 'No DOM for observation. Please use' +\
                             'start(sFilepathroadcast, sFilepathObservation) '\
                             +'before any call to vix_roadcast_observation'
            
        ############## Roadcast #########################################
        lXrc = self.metro_xml_libxml2.\
               get_nodes_in_list(self.domRoadcast, '//roadcast-time')

        lSTrc = self.metro_xml_libxml2.\
                 get_nodes_in_list(self.domRoadcast, '//st')
        npSTrc = numpy.array(vix_util.tranform_list_string_in_number(lSTrc))

        lATrc = self.metro_xml_libxml2.\
                 get_nodes_in_list(self.domRoadcast, '//at')
        npATrc =  numpy.array(vix_util.tranform_list_string_in_number(lATrc))
            
        lTDrc = self.metro_xml_libxml2.\
                     get_nodes_in_list(self.domRoadcast, '//td')
        npTDrc = numpy.array(vix_util.tranform_list_string_in_number(lTDrc))
        fLat = float((self.metro_xml_libxml2.\
                get_nodes_in_list(self.domRoadcast, '//latitude'))[0])
        fLon = float((self.metro_xml_libxml2.\
                get_nodes_in_list(self.domRoadcast, '//longitude')[0]))

        npFrozingPoint = numpy.zeros(len(lXrc))
            
        ############## Observation #########################################
        lXobs = self.metro_xml_libxml2.\
                get_nodes_in_list(self.domObservation, '//observation-time')
        
        lSTobs = self.metro_xml_libxml2.\
                  get_nodes_in_list(self.domObservation, '//st')
        npSTobs = numpy.array(vix_util.tranform_list_string_in_number(lSTobs))
        lATobs = self.metro_xml_libxml2.\
                  get_nodes_in_list(self.domObservation, '//at')
        npATobs = numpy.array(vix_util.tranform_list_string_in_number(lATobs))
        lTDobs = self.metro_xml_libxml2.\
                  get_nodes_in_list(self.domObservation, '//td')
        npTDobs = numpy.array(vix_util.tranform_list_string_in_number(lTDobs))
        if sFilename == None:
            sFilename = 'road_obs.png'

        r.png(sFilename, self.nPngWidthST, self.nPngHeightST, bg='white')
            
        self.__set_plot_attributes(npATrc, npSTrc, npTDrc,\
                            npATobs, npSTobs, npTDobs, npFrozingPoint)


        if self.domRoadcast != None:
            self.__plot_roadcast(lXrc, npATrc, npSTrc, npTDrc, \
                                 npFrozingPoint, fLat, fLon)
        if self.domObservation != None:
            self.__plot_observation(lXobs, npATobs, npSTobs, npTDobs)

        sMessage =  _("File %s written") % (sFilename)
        print(sMessage)
        r.dev_off()

    ####################################################
    # Name: set_plot_attributes
    #
    # Parameters: [[I] 2 Numeric.Array , represent the x values (times)
    #                    of observations and roadcast.
    #              [[I] 6  Numeric.Array, the value to be plot.
    #
    # Returns: None
    #
    # Description: Set the attributes of the plot.
    #  1- Write the number of axis horizontally.
    #  2- Set the max and min of the axis.
    #
    # Notes: 
    #
    # Revision History:
    #  Author		Date		Reason
    # Miguel Tremblay      November 24th 2004   
    #####################################################
    def __set_plot_attributes(self, npATrc, npSTrc, npTDrc,\
                            npATobs, npSTobs, npTDobs, npFrozingPoint):
        # Axis
        r.par(las=1) # Axis values always horizontals
        # Compute the min and max for all the value
        self.nYMinGraph = int(vix_util.vix_min(-40, [npATrc, npSTrc, npTDrc,\
                                   npATobs, npSTobs, npTDobs, npFrozingPoint]))
        self.nYMinGraph = self.nYMinGraph - self.nYMinGraph%5
        if self.nYMinGraph > 0:
            self.nYMinGraph = 0
            
        self.nYMaxGraph = int(vix_util.vix_max(50, [npATrc, npSTrc, npTDrc,\
                                                    npATobs, npSTobs, npTDobs,\
                                                    npFrozingPoint])+0.5)
        self.nYMaxGraph = self.nYMaxGraph + (5-self.nYMaxGraph%5)
        if self.nYMaxGraph > 50:
            self.nYMaxGraph = 50
    
    def __plot_roadcast(self, lXrc, npATrc, npSTrc, npTDrc, npFrozingPoint, fLat, fLon):
    
        ###########################################
        # Plot the result
        rXrc_axis = r.strptime(lXrc, "%Y-%m-%dT%H:%M%Z")
        r.plot(rXrc_axis, npSTrc, yaxs='i', \
               ylim=[self.nYMinGraph, self.nYMaxGraph], type='l', \
               xlab='Time (UTC)', ylab='Temperature �C', main='Roadcast'\
               ,col='blue',  xaxt='n', yaxt='n', lty=0)

        rRange_hour = r.as_POSIXct(r.round(r.range(rXrc_axis), "hours"))
        rHour = r.seq(rRange_hour[0], rRange_hour[1], by="hours")
        rHour = r.c(rHour, rRange_hour[1])
        
        # Get the sunrise and the sunset for each day
        lUsr = r.par("usr")
        rYdomain = r.c(lUsr[2], lUsr[3], lUsr[2])
        rRange_day = r.as_POSIXct(r.range(rXrc_axis))
        rdays = r.seq(rRange_day[0], rRange_day[1], by="day")
        # Unable to add the last day with the r.seq method.
        # Use the brut force method.
        rdays = r.c(rdays, rRange_day[1])

        for rday in rdays:
            # Transform R object in iso 8601
            rpy.set_default_mode(rpy.BASIC_CONVERSION)
            sDay = r.format(rday, format="%Y-%m-%d")
            rpy.set_default_mode(rpy.NO_CONVERSION)
            (sSunrise, sSunset) = vix_util.get_sunrise_sunset(sDay, fLat, fLon)

            rSunrise = r.strptime(sSunrise, "%Y-%m-%dT%H:%M:%S")
            rSunset = r.strptime(sSunset, "%Y-%m-%dT%H:%M:%S")
             # Compute half-way between sunset and sunrise for the top of the triangle
             #  displayed on the graph. Use ctime
            sMiday = vix_util.get_half_time(sSunrise, sSunset)
            rMiday = r.strptime(sMiday, "%Y-%m-%dT%H:%M:%S")
            # X-domain of the triangle
            rXdomain = r.c(rSunrise, rMiday, rSunset)
            r.polygon(rXdomain, rYdomain, col='lemonchiffon',  border='NA')

        ### Grid
        rXdomain = r.c(lUsr[0], lUsr[1])
        for i in range(self.nYMinGraph, self.nYMaxGraph, 5):        
            if i == self.nYMinGraph or i == self.nYMaxGraph:
                continue
            rYdomain = r.c(i,i)
            r.lines(rXdomain, rYdomain, col='gray70') # Horizontal lines
        # Line at 0�C.
        rYdomain = r.c(0,0)
        # Horizontal lines
        r.lines(rXdomain, rYdomain, lwd=2, lty='dashed', col='black')
        rYdomain = r.c(lUsr[2], lUsr[3])
        for nHour in rHour:
            rXdomain = r.c(nHour, nHour)
            r.lines(rXdomain, rYdomain, col='gray70') # Vertical lines
        ####

        self.create_x_axis(rXrc_axis, \
                           self.nYMinGraph- (self.nYMaxGraph-self.nYMinGraph)/10)
        # Lines
        r.lines(rXrc_axis, npSTrc, col='blue',# lty='dashed', \
                lwd=self.nLineWidth)# st roadcast
        r.lines(rXrc_axis, npATrc, col='red', #lty="dashed", \
                lwd=self.nLineWidth)# at roadcast
        r.lines(rXrc_axis, npTDrc, col='green',# lty='dashed',\
                lwd=self.nLineWidth)# td roadcast

        lVerticalAxis = list(range(self.nYMinGraph, self.nYMaxGraph, 5))
        # Those two axis for the tick at every 5 �C        
        r.axis(2, at=r.c(lUsr[2],lVerticalAxis))
        r.axis(4, at=r.c(lUsr[2],lVerticalAxis))
        # Those two to redraw the axis over the triangle of the sunrise sunset
        r.axis(2, at=r.c(lUsr[2],lUsr[3]))
        r.axis(4, at=r.c(lUsr[2],lUsr[3]))

    def __plot_observation(self, lXobs, npATobs, npSTobs, npTDobs):
        rXobs_axis = r.strptime(lXobs, "%Y-%m-%dT%H:%M%Z")
        # Plot the observation
        ## ST observed
        r.points(rXobs_axis, npSTobs, col='blue', lwd=self.nLineWidth)
        ## AT observed
        r.points(rXobs_axis, npATobs, col='red', lwd=self.nLineWidth)
        ## TD observed
        r.points(rXobs_axis, npTDobs, col='green', lwd=self.nLineWidth)

        # Print a brow line where the observations ends.
        lUsr = r.par("usr")
        sLastObs = lXobs[len(lXobs)-1]
        rLastObs = r.strptime(sLastObs, "%Y-%m-%dT%H:%M%Z")
        rXdomain = r.c(rLastObs, rLastObs)
        rYdomain = r.c(lUsr[2], lUsr[3])
        r.lines(rXdomain, rYdomain, col='Maroon', lty='dashed',\
                lwd=self.nLineWidth)
        
        return

    ###########################################################
    # X axis with date is particularly awful.
    # Create it once here.
    ##########################################################
    def create_x_axis(self,rXaxis, fYpos):
        
        # Get the range of the hours for this array
        rRange_hour = r.as_POSIXct(r.round(r.range(rXaxis), "hours"))
        rHour = r.seq(rRange_hour[0], rRange_hour[1], by="hours")
        
        # Draw the hours
        r.axis_POSIXct(1, rHour, at=rHour, format="%Hh")

        # Place dates at the extremity
        rAxisExtremity = r.range(rXaxis)
        rDateLabels = r.as_character(r.format(rAxisExtremity,"%d %b"))
        lUsr = r.par('usr')
        r.axis(1,  at=r.c(lUsr[0], lUsr[1]), pos=fYpos,
               tick=0, labels=rDateLabels)
        # Place date at midnight
        rDay = r.as_POSIXct(r.round(rAxisExtremity, "day"))
        rDateLabels = r.as_character(r.format(rDay[0],"%d%b"))
        r.axis(1, at=rDay[0], pos=fYpos, tick=0,labels=rDateLabels )
        rDateLabels = r.as_character(r.format(rDay[1],"%d%b"))
        r.axis(1, at=rDay[1], pos=fYpos, tick=0,labels=rDateLabels )

    
def main():

    nNbrArgs = len(sys.argv)

    if nNbrArgs < 3:
        print("Usage vix_roadcast.py roadcast.xml observation.xml\n")
        print("or\n")
        print("vix_roadcast.py roadcast.xml observation.xml filenamePlot.png")
        return
    
    vix_rc_obs = Vix_Roadcast_Observation(sys.argv[1], sys.argv[2])

    if len(sys.argv) == 3:
        vix_rc_obs.plot_roadcast_and_observation()
    elif len(sys.argv) == 4:
        vix_rc_obs.plot_roadcast_and_observation(sys.argv[3])


if __name__ == "__main__":
    main()

