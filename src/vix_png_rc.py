#!/usr/bin/env python
# # -*- coding:iso-8859-1  -*-
#
# Vix : Visualization of XML
# ViX is Free and is provided by the Government of Canada
# Copyright (C) 2006 Environment Canada
#
#  Documentation: http://documentation.wikia.com/wiki/ViX
#
#
# Code contributed by:
#  Miguel Tremblay - Canadian meteorological center
#
#  $LastChangedDate$
#  $LastChangedRevision$
#
########################################################################
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
#

"""
Name:	       vix_png_rc.py
Description: Generate the png graphics for all the roadcast.
 
Notes: 

Author: Miguel Tremblay
Date: January 12th 2004
"""
import sys
sys.path.append('./ripp4x/')

import vix_roadcast
from vix_ripp4x_roadcast import Vix_ripp4x_roadcast
from vix_png import Vix_png

class Vix_png_rc(Vix_png):

    sSubType = 'roadcast/'
    sKeyword = 'roadcast'
    nStartDay = 21

    def __init__(self, lArgs):
        Vix_png.__init__(self, lArgs)
        
    def process_vix(self, sRoadcastPath, sPngFilename):
        vix_rc = vix_roadcast.Vix_Roadcast(sRoadcastPath)
        vix_rc.plot_all(sPngFilename)
        vix_rc.close()
        
    def process_ripp4x(self, sRoadcastPath, sRipp4xFilename, dPath):
        vix_ripp4x_roadcast = Vix_ripp4x_roadcast\
                              (sRipp4xFilename)
        vix_ripp4x_roadcast.process(sRoadcastPath)
        for sElement in list(dPath.keys()):
            vix_ripp4x_roadcast.set_path(sElement, \
                                         dPath[sElement])
        vix_ripp4x_roadcast.write_xml_file()

    



if __name__ == "__main__":
    import sys

    lArgs = ['']*4

    if sys.argv[1] == 'all':
        lStation = ['oav', 'ocy', 'oce', 'ocv', 'obw', 'oby']
    else:
        lStation = [sys.argv[1]]

    for sStation in lStation:
        for i in range(1, len(sys.argv)):
            lArgs[i-1] = sys.argv[i]
        lArgs[0] = sStation
        vix = Vix_png_rc(lArgs)
        vix.process()




