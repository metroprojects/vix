#!/usr/bin/env python
# # -*- coding:iso-8859-1  -*-
####################################################
# Name:	       merge_observation.py
# Description: Take an serie of xml observation file and creation only one.
#  
# Notes: Using SAX to perform this task.
# Auteur: Miguel Tremblay
# Date: December 8th 2004
####################################################

import sys
import os

sOpenListTag ='<measure-list>'
sCloseListTag = '</measure-list>'


def main():
    global sFileOutput
    
    if len(sys.argv) < 4:
        print "usage", sys.argv[0], "output.xml observation1.xml observation2.xml ..."
        return

    # Read the first file and find the position of 
    sFile = open(sys.argv[2]).read()
    sOutput = sys.argv[1]
    nInsertPosition = sFile.rfind(sCloseListTag)
    sEndFile = sFile[nInsertPosition:]
    print sEndFile

    # Parse all file
    lObservationFiles = sys.argv[2:]
    for observationFile in lObservationFiles:
        print "Processing", observationFile
        sCurrentObservation = open(observationFile).read()
        # Find indice of  <measure-list> and </measure-list>
        nFirstIndice = sCurrentObservation.find(sOpenListTag) + len(sOpenListTag)
        nLastIndice = sCurrentObservation.rfind(sCloseListTag)
        sFile = sFile[:nInsertPosition] +\
                sCurrentObservation[nFirstIndice:nLastIndice]
        nInsertPosition = nInsertPosition + (nLastIndice-nFirstIndice)

    # Terminate file 
    sFile = sFile + sEndFile
    # Write file
    file_output = open(sOutput,'w')
    file_output.write(sFile)

if __name__ == "__main__":
    main()
