#!/usr/bin/env python
# # -*- coding:iso-8859-1  -*-
####################################################
# Name:	       merge_observation_24h.py
# Description: Starting files are 12h observations.
#  Merge 2 files to obtain 24h file.
#  
# Notes: 24h files must start in the morning
# Auteur: Miguel Tremblay
# Date: May 6th 2005
####################################################

import sys
import os

sOpenListTag ='<measure-list>'
sCloseListTag = '</measure-list>'
sOpenObservationTag = '<observation-time>'
sCloseObservationTag = '</observation-time>'

def main():
    global sFileOutput
    
    if len(sys.argv) < 3:
        print "usage", sys.argv[0], "directory_in directory out"
        return

    sObsDirectoryIn = sys.argv[1]
    sObsDirectoryOut = sys.argv[2]
    
    # Read all files in the directory        
    if not os.path.exists(sObsDirectoryIn):
        print "Directory '%s' does not exist!" % (sObsDirectoryIn)
        exit(1)

    # Parse every file
    lFiles = os.listdir(sObsDirectoryIn)
    lFiles.sort()
    for i in range(0, len(lFiles)-1):
        sFile = lFiles[i]
#        print "Checking file '%s'" % (sFile)
        
        sFilepath = sObsDirectoryIn + '/' + sFile

        if sFile.find('.xml') < 0: # Not an xml file
            continue

        # Check if the file can be considerated as the first one 
        if start_morning(sFilepath):
#            print "File '%s' is in the morning" %(sFilepath)
            sNextFile = lFiles[i+1]
            sFilepathNext = sObsDirectoryIn + '/' + sNextFile
            if one_day_difference(sFilepath, sFilepathNext):
                concat_files(sFile, sFilepath, sFilepathNext, sObsDirectoryOut)


#####################################################################
# Get the date of the first observation, given a filepath
####################################################################
def get_first_obs_date(sFilepath):
    sFile = open(sFilepath).read()

    # Fetch the first observation time
    nIndiceStart = sFile.find(sOpenObservationTag) + len(sOpenObservationTag)
    nIndiceEnd = sFile.find(sCloseObservationTag)

    sDate = sFile[nIndiceStart:nIndiceEnd]

    return sDate

####################################################################
# Check if the starting hour is before noon. If so, return true, else
#  returns false.
####################################################################
def start_morning(sFilepath):
    
    sDate = get_first_obs_date(sFilepath)

    nHour = int(sDate[sDate.rfind('T')+1:sDate.rfind(':')])

    if nHour < 17 :
        return True
    else:
        return False

###################################################################
# Verify if the file are only one day or less difference, based on the
#  filename.
##################################################################
def one_day_difference(sFilepath1, sFilepath2):

    sDate1 = get_first_obs_date(sFilepath1)
    sDate2 = get_first_obs_date(sFilepath2)

    nDayOne = int(sDate1[sDate1.find('T')-2:sDate1.find('T')])
    nDayTwo = int(sDate2[sDate2.find('T')-2:sDate2.find('T')])

#    print nDayOne, nDayTwo

    if nDayTwo - nDayOne == 0:
        return True
    else:
        return False

##################################################################
# Concat the files. Put the all the children of measure-list node
#  of the first file and insert them in the measure-list node of the
#  first file. Save the result in sDirectory with the filename of the
#  first file.
#################################################################
def concat_files(sFilename, sFilepath1, sFilepath2, sDirectory):

    # Read the first file and find the position of 
    sFile = open(sFilepath1).read()
    sOutput = sDirectory + '/' + sFilename

    nInsertPosition = sFile.rfind(sCloseListTag)
    sEndFile = sFile[nInsertPosition:]

    sFile2 = open(sFilepath2).read()
    # Find indice of <measure-list> and </measure-list>
    nFirstIndice = sFile2.find(sOpenListTag) + len(sOpenListTag)
    nLastIndice = sFile2.rfind(sCloseListTag)
    sFile = sFile[:nInsertPosition] +\
            sFile2[nFirstIndice:nLastIndice]

    # Terminate file 
    sFile = sFile + sEndFile
    # Write file
    if not os.path.exists(sDirectory):
        os.mkdir(sDirectory)
    file_output = open(sOutput,'w')
    file_output.write(sFile)

###################################################################
# Given a date in the format yyyy-mm-dayThh:mm   return the hour
##################################################################

    
if __name__ == "__main__":
    main()
