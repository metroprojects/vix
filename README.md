# ViX README file

__ViX__ stands for __Vi__sualization of __X__ML.

It has been devevelopped to create images from the ouput of the [METRo software](https://framagit.org/metroprojects/metro/wikis/home). 

Please see the [ViX documentation](https://framagit.org/metroprojects/metro/wikis/Vix) for more information.


## Contact us 

You can join the ViX developer team at the address: 
[metro-users@framalistes.org](mailto:metro-users@framalistes.org)
